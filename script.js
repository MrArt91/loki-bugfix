if(window.toolbar!=undefined)
{
  toolbar[toolbar.length] = {"type":"format",
                             "title":"Prolog",
                             "icon":"../../plugins/loki/prolog.png",
                             "key":"",
                             "open": "<pl>",
                             "close":"</pl>"};
}

if(window.toolbar!=undefined)
{
	  toolbar[toolbar.length] = {"type":"format",
	                             "title":"Category",
	                             "icon":"../../plugins/loki/category.png",
	                             "key":"",
	                             "open": "[[category:",
	                             "close":"]]"};
}

if(window.toolbar!=undefined)
{
	  toolbar[toolbar.length] = {"type":"format",
	                             "title":"Relation",
	                             "icon":"../../plugins/loki/relation.png",
	                             "key":"",
	                             "open": "[[",
	                             "close":"::]]"};
}


if(window.toolbar!=undefined)
{
	  toolbar[toolbar.length] = {"type":"format",
	                             "title":"Attribute",
	                             "icon":"../../plugins/loki/attribute.png",
	                             "key":"",
	                             "open": "[[",
	                             "close":":=]]"};
}


if(window.toolbar!=undefined)
{
	  toolbar[toolbar.length] = {"type":"format",
	                             "title":"Ask",
	                             "icon":"../../plugins/loki/ask.png",
	                             "key":"",
	                             "open": "{{#ask: [[]]",
	                             "close":"}}"};
}

if(window.toolbar!=undefined)
{
	  var sparql_arr = new Array(); /* array[key]= insertion string , value = icon filename. */
	  sparql_arr["<pl format=\"sparql\">\nPREFIX wiki : <> \nSELECT ?#name# ?#name2# \nWHERE { \n ?#name# a \"#category#\" ;\n   wiki:#attribute# \"#value#\" .\n ?#name2# wiki:#relation# ?#name# .\n OPTIONAL { ?#name# wiki:#attribute# \"#value2#\" } . \n FILTER ( #condition1# && #condition2# ) \n}\nORDER BY ?#name#, DESC(?#name2#)\n</pl>"]          ="sparql_select.png";
	  sparql_arr["<pl format=\"sparql\">\nPREFIX wiki : <> \nASK { \n ?#name# a \"#category#\" .\n ?#name2# wiki:#property# ?#name# \n}\n</pl>"]      ="sparql_ask.png";
	  sparql_arr["<pl format=\"sparql\">\nPREFIX wiki : <> \nDESCRIBE ?#name#\nWHERE { \n ?#name# a \"#category#\" ; \n  wiki:#relation# <#value#> ;\n  wiki:#attribute# \"#value2#\" \n }\n</pl>"]="sparql_describe.png";
	  toolbar[toolbar.length] = {"type":"picker",
		                         "title":"SPARQL (SELECT / ASK / DESCRIBE)",
		                         "icon":"../../plugins/loki/sparql.png",
		                         "key":"",
		                         "list": sparql_arr,
		                         "icobase":"../plugins/loki/"};

	 
}


function jsEscape(text){
    var re=new RegExp("\\\\","g");
    text=text.replace(re,"\\\\");
    re=new RegExp("'","g");
    text=text.replace(re,"\\'");
    re=new RegExp('"',"g");
    text=text.replace(re,'\\"');
    re=new RegExp("\\\\\\\\n","g");
    text=text.replace(re,"\\n");
    return text;
}

