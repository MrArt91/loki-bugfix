<?php
/**
 * Loki Syntax Plugin
 * 
 * @license	 GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author		GEIST Research Group <geist@agh.edu.pl>
 * @version	 1.0
 */

if(!defined('DOKU_INC')) define('DOKU_INC',realpath(dirname(__FILE__).'/../../').'/');
require_once(DOKU_INC.'inc/init.php');
require_once(DOKU_INC.'inc/geshi.php');
if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');
require_once(DOKU_PLUGIN.'syntax.php');
include_once DOKU_INC.'lib/plugins/loki/utl/loki_utl.php';
include_once DOKU_INC.'lib/plugins/loki/config.php';

/**
 * All DokuWiki plugins to extend the parser/rendering mechanism
 * need to inherit from this class
 */
class syntax_plugin_loki extends DokuWiki_Syntax_Plugin 
{


	function getInfo()
	{
		return array (
	 		'author' => 'GEIST Research Group',
			'email' => 'geist@agh.edu.pl',
			'date' => '2011-04-13',
			'name' => 'Loki Plugin (syntax component)',
			'desc' => 'Semantic plugin for DokuWiki',
			'url'	 => 'http://loki.ia.agh.edu.pl/download/loki.zip',
			);
  	}
  
  	function canRender($format)
  	{
	 	return ($format=='xhtml');
  	}

	/**
		* What kind of syntax are we?
		*/
  	function getType()
  	{
	 	return 'protected';
  	}
	
  	/**
		* Where to sort in?
		*/ 
  	function getSort()
  	{
			return 100;
  	}

	/**
		* Connect pattern to lexer
		*/
	function connectTo($mode)
	{
 		$this->Lexer->addEntryPattern('<pl[^>]*?[^/]>(?=.*?</pl>)',$mode,'plugin_loki'); 
 		$this->Lexer->addSpecialPattern('<pl[^>]*?/>',$mode,'plugin_loki'); 
	}


 	function postConnect() 
 	{
	 	$this->Lexer->addExitPattern('</pl>','plugin_loki');
  	}

  	/**
		* Handle the match
		*/
 	function handle($match, $state, $pos, &$handler)
 	{
		  switch ($state) 
		  {
            case DOKU_LEXER_SPECIAL:
		  	case DOKU_LEXER_ENTER :
				$list = preg_split('/([a-z]*="[^"]*")/', substr($match,4,-1), -1, PREG_SPLIT_DELIM_CAPTURE);
		
				foreach ($list as $param)
				{ 
					if (strstr($param,"file="))
						$file=$param;
					elseif (strstr($param,"scope="))
						$scope=$param;
					elseif (strstr($param,"url="))
						$url=$param;
					elseif (strstr($param,"goal="))
						$goal=$param;
					elseif (strstr($param,"cache="))
						$cache=$param;
					elseif (strstr($param,"rdf="))	// kept only for backward compatibility.
						$rdf=$param;				// use of the "format" attribute recommended instead.
					elseif (strstr($param,"msgerr="))
						$msgerr=$param;
					elseif (strstr($param,"trace="))
						$trace=$param;
					elseif (strstr($param,"format="))
						$format=$param;
				}

				$scope=ltrim($scope,'scope=');
				$scope=trim($scope,'"');		
				$goal=ltrim($goal,'goal=');
				$goal=trim($goal,'"');		
				$file=ltrim($file,'file=');
				$file=trim($file,'"');
				$file=str_replace(':','/',$file);
				$url=ltrim($url,'url=');
				$url=trim($url,'"');
				$cache=ltrim($cache,'cache=');
				$cache=trim($cache,'"');		
				$rdf=ltrim($rdf,'rdf=');
				$rdf=trim($rdf,'"');	
				$msgerr=ltrim($msgerr,'msgerr=');
				$msgerr=trim($msgerr,'"');
				$trace=ltrim($trace,'trace=');
				$trace=trim($trace,'"');
				$format=ltrim($format,"format=");
				$format=trim($format,'"');
			
					 return array($state, array($goal,$scope,$msgerr,$trace, $file, $url,$cache, $rdf, $format));
					 
			 	case DOKU_LEXER_UNMATCHED :  
			 		return array($state, $match);
			 	
			 	case DOKU_LEXER_EXIT : 
			 		return array($state, '');
		  }
		  
		  return array();
	 }


	function render($mode, &$renderer, $data) 
	{
		global $conf, $goal, $scope, $file, $url, $code, $cache, $rdf, $msgerr, $trace, $format;
		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl;
		$renderer->info["cache"] = false;
			
		if($mode == 'xhtml')
		{
		 	list($state, $match) = $data;
			  switch ($state) 
			  {
			  	case DOKU_LEXER_ENTER :
						 list($goal, $scope, $msgerr, $trace, $file, $url, $cache, $rdf, $format) = $match;
						 break;
					case DOKU_LEXER_UNMATCHED :		
					$code = $match;
					break;
                    case DOKU_LEXER_SPECIAL:
						 list($goal, $scope, $msgerr, $trace, $file, $url, $cache, $rdf, $format) = $match;
						 $code = "";
					case DOKU_LEXER_EXIT :	
						$hash = md5(serialize($goal.time()));
				
					//getting format of the match
					$format = strtolower($format);
					if (!in_array($format,array("rdf","prolog","sparql")))	//all alowed formats
					{
						if (strtolower($rdf)=="true") $format="rdf";
						else $format = "prolog";
					}
				
					//MO - setting trace parameters
					if(strlen($trace)!=0)				
					{
						$tracelist = explode(',', $trace);
						$trace = '';
						foreach ($tracelist as $tracepred)
						{
							$trace .= 'trace('.$tracepred.'),';
						}
					}

					//MO - setting the destination for error messages
					if(strlen($msgerr)!= 0)
					{
						if (strstr($msgerr,"ignore"))  
							$msgerr = ' 2>/dev/null ';
						elseif (strstr($msgerr,"display"))
							$msgerr = ' 2> '.TEMP_DIR.'disperrors'.$hash;
						elseif (strstr($msgerr,"tofile"))
							$msgerr = ' 2>> '.TEMP_DIR.'errors-'.getID().'-'.Date('Ymd').'.err'; 
						else $msgerr = ' 2>/dev/null '; //akcja domyslna - ignore

					}					  
					  	
					if(strlen($file)!=0)
					{
						$code=file_get_contents("$conf[mediadir]/".str_replace(":","/",$file));
					}
					

					if(strlen($url)!=0)
					{
						//reading url using curl
						//$tmpfname = tempnam("/tmp", "dokuwiki.curl");
							exec('/usr/bin/curl '.$url.' > '.TEMP_DIR.'dokuwiki.curl'.$hash);
			  			exec('/usr/bin/curl '.$url.' > '.TEMP_DIR.'dokuwiki.curl'.$hash);			 
						$code=file_get_contents(TEMP_DIR."dokuwiki.curl".$hash);
						$content=file_get_contents(TEMP_DIR."dokuwiki.curl".$hash);		
						@unlink(TEMP_DIR."dokuwiki.curl".$hash);		 
					}		
						
					if($format == "rdf")
					{

						io_saveFile(TEMP_DIR."dokuwiki.rdf".$hash,$code);  //using dokuWiki framework
						
						$fullgoal = "[library(rdf)],load_rdf('".TEMP_DIR."dokuwiki.rdf".$hash."', H),checklist(assert, H) ,".$goal;
						if(strlen($goal)==0) { $fullgoal = rtrim($fullgoal, ',').'.'; }
						
							exec('LANG=pl_PL.utf-8; /usr/bin/swipl -q -g "'.$fullgoal.'" -t halt > '.TEMP_DIR.'dokuwiki.loki'.$hash.$msgerr) ;
						//	exec('LANG=pl_PL.utf-8; /usr/bin/swipl -q -g "'.$fullgoal.'" -t halt > '.TEMP_DIR.'dokuwiki.prolog'.$hash.$msgerr) ;
							if ( $GLOBALS['ACT'] == 'preview' )
							{
								//geshi - kolorowanie składni	 		
							$geshi = new GeSHi($code, 'xml');
							$res = $geshi->parse_code();
							$renderer->doc .= $res;
							}
								
							$retval=file_get_contents(TEMP_DIR."dokuwiki.loki".$hash);
						//	$retval=file_get_contents(TEMP_DIR."dokuwiki.prolog".$hash);
						if(file_exists(TEMP_DIR."disperrors".$hash))
							$renderer->doc .= "\n".file_get_contents(TEMP_DIR."disperrors".$hash);

							@unlink(TEMP_DIR."dokuwiki.loki".$hash);	 
						//	@unlink(TEMP_DIR."dokuwiki.prolog".$hash);	 
						@unlink(TEMP_DIR."dokuwiki.rdf".$hash);	 
				
						 if ( $GLOBALS['ACT'] == 'preview' && strlen($goal)!=0 )
						{ 
							//geshi - kolorowanie składni	 		
							$geshi = new GeSHi($goal, 'prolog',getcwd()."/"."lib/plugins/loki");
							$res = $geshi->parse_code();
							$new_res = ereg_replace  ( "</pre>"  , " -> <br>".$retval."</pre>"	, $res  );
							$renderer->doc .= $new_res;
						}
						else if ( strlen($goal)!=0  )
						{
							$renderer->doc .= $retval;
						}
						return true;
					} 
					elseif ($format == 'sparql'){
						include_once(DOKU_INC."/lib/plugins/loki/utl/loki_utl_sparql.php");
						$utls = new LokiUtlSparql;
						$resul = $utls->process_sparql($code); $code="";
						$renderer->doc.=$resul;
						return true;
					}		
				
					
					elseif( $format == "prolog" && strcmp($cache,"false")!=0 &&  $GLOBALS['ACT'] != 'preview')
					{				
						$path=getID();
						$path_ex = explode(":",getID());
						$page=end($path_ex).".txt";
						
						$path= $path.".txt";
						$path = str_replace($page,"",$path);	
						$path=rtrim($path,":");
							
						$path=str_replace(":","/",$path);
						if(strlen($path)!=0)
						{
							$path .= "/";
						}
						
						//$fullpath="/var/www/wiki/lib/plugins/tmp/".$path;
	
						$p=rtrim($conf[mediadir],"data/media");
						$fullpath=TEMP_DIR."/".$path;
									
						$utl->recursive_mkdir($fullpath);
				
						//jezeli strona istniala juz wczesniej
						if(file_exists($fullpath.$page))
						{		
							//dopisz
							$old = file_get_contents($fullpath.$page);		
							$new = $old.$code;
							file_put_contents($fullpath.$page,$new);		
						
							//merguj
							$filecont = file_get_contents($fullpath.$page);		
							$filetab=preg_split("/(\s*%[^\n]*\n|[^\(.]*\([^\)]*\)[^\.]*\.|[^\.]*\.)/",$filecont,-1,PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);	
							$page_source = file_get_contents($conf[datadir]."/".$path.$page);
							
							foreach($filetab as $curr_line)
							{ 
								$curr_line = trim($curr_line);
								if($curr_line=="") continue;
								if(strstr($page_source,$curr_line)!=false || strstr($curr_line,"wiki_")!=false || strstr($code,$curr_line)!=false)
								{
									if(strstr($out,$curr_line)==false || strstr($curr_line,"wiki_")!=false)
										$out.=$curr_line."\n";
								}
							}
						
							io_saveFile($fullpath.$page, $out);  //using dokuWiki framework
						}
						else
						{
							$out=$code;
						
							io_saveFile($fullpath.$page, trim($out));  //using dokuWiki framework
						}
						
					}
				
					// If preview add colored code to output
				  		if ( $GLOBALS['ACT'] == 'preview' && strlen(trim($code))!=0 )
					{ 
						//geshi - kolorowanie składni	 		
						$geshi = new GeSHi($code, 'prolog',getcwd()."/"."lib/plugins/loki");
						$res = $geshi->parse_code();
						$renderer->doc .= $res;
					}
					
					if(strlen($scope)!=0 && strlen($goal)!=0)
					{	
						$scope = str_replace(":","/",$scope);
						$scope = TEMP_DIR."/".$scope;
						
						//$renderer->doc .= 'grep ".*" -rh  $(grep -rl ".*" /tmp/test | grep "'.$scope.'")  > /tmp/dokuwiki.code';
						//return true;
						exec('echo ":- style_check(-discontiguous)." > '.TEMP_DIR.'dokuwiki.code'.$hash);
						exec('grep ".*" -rh  $(grep -rl ".*" '. TEMP_DIR.' | grep "'.$scope.'")  >> '.TEMP_DIR.'dokuwiki.code'.$hash);
						//exec("grep '.*' ".$scope." -r -h  > /tmp/dokuwiki.code") ;	 			
						
					}
					else
					{
						io_saveFile(TEMP_DIR."dokuwiki.code".$hash, ":- style_check(-discontiguous).\n".$code); 
					}
					 //swipl -f pr.pl -s g.pl -g start -t halt
			
					//exec('ls > /tmp/dokuwiki.loki');
					if( strlen($goal)!=0)
					{
						//exec('LANG=pl_PL.utf-8; /usr/bin/swipl -f '.TEMP_DIR.'dokuwiki.code -g "'.$goal.'" -t halt > '.TEMP_DIR.'dokuwiki.loki') ;
				  		//exec('LANG=pl_PL.utf-8; /usr/bin/swipl -q -s '.TEMP_DIR.'dokuwiki.code'.$hash.' -g "'.$trace.$goal.'" -t halt > '.TEMP_DIR.'dokuwiki.loki'.$hash.$msgerr) ;
						exec('LANG=pl_PL.utf-8; /usr/bin/swipl -q -s '.TEMP_DIR.'dokuwiki.code'.$hash.' -g "'.$trace.$goal.'" -t halt > '.TEMP_DIR.'dokuwiki.loki'.$hash.$msgerr) ;
						$retval=file_get_contents(TEMP_DIR."dokuwiki.loki".$hash);
						if(file_exists(TEMP_DIR."disperrors".$hash))
							$renderer->doc .= "\n".file_get_contents(TEMP_DIR."disperrors".$hash);
					}
					
					@unlink(TEMP_DIR."dokuwiki.code".$hash);
						@unlink(TEMP_DIR."dokuwiki.loki".$hash);
					@unlink(TEMP_DIR.'disperrors'.$hash);
				  
					
					//$renderer->doc .= "	".$retval;
					  if ( $GLOBALS['ACT'] == 'preview' && strlen($goal)!=0 )
					{ 
						//geshi - kolorowanie składni	 		
						$geshi = new GeSHi($goal, 'prolog',getcwd()."/"."lib/plugins/loki");
						$res = $geshi->parse_code();
						$new_res = ereg_replace  ( "</pre>"  , " -> <br>".$retval."</pre>"	, $res  );
						$renderer->doc .= $new_res;
					} 
				
					else if ( !(stripos ($goal, "wiki_") ==FALSE)  &&  substr_count($goal,"write(") == 1 && strlen($trace)<=0)
					{
						$results = explode("\n", $retval);		
						foreach ($results as $item)
						{
							if( strlen($item)!=0 ) 	
							{ 
								$renderer->doc .= "	".$renderer->internallink($item, NULL, NULL, true);	
							}
						}	 
					} 
					else if ( strlen ($goal) != 0 )
					{		
						$renderer->doc .= "	".str_replace("\n","<br>",$retval);
					
					}
	
					return true; 	
			  }
			
			  return true;
		}
		
		 return false;
	}

}



?>
