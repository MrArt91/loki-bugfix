%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% loki_utl.pl - Loki utils
%
% author: GEIST Research Group
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

wiki_subcategory(dummy__,dummy__).
wiki_subrelation(dummy__,dummy__).
wiki_subattribute(dummy__,dummy__).


is_wiki_subcategory(Subcategory, Category) :-
	wiki_subcategory(Subcategory, Category).

is_wiki_subcategory(Subcategory, Category) :-
	wiki_subcategory(Subcategory, Cat),
	is_wiki_subcategory(Cat, Category).

is_wiki_category(Page, Category) :- 
	wiki_category(Page, Category).

is_wiki_category(Page, Category) :-
	wiki_category(Page, Cat),
	is_wiki_subcategory(Cat, Category).

%%%

is_wiki_subattribute(Subattribute, Attrribute) :-
	wiki_subattribute(Subattribute, Attrribute).

is_wiki_subattribute(Subattribute, Attrribute) :-
	wiki_subattribute(Subattribute, Att),
	is_wiki_subattribute(Att, Attrribute).

is_wiki_attribute(Page, Attribute, Value) :- 
	wiki_attribute(Page, Attribute, Value).

is_wiki_attribute(Page, Attribute, Value) :-
	wiki_attribute(Page, Att, Value),
	is_wiki_subattribute(Att, Attribute).

%%%

is_wiki_subrelation(Subrelation, Relation) :-
	wiki_subrelation(Subrelation, Relation).

is_wiki_subrelation(Subrelation, Relation) :-
	wiki_subrelation(Subrelation, Rel),
	is_wiki_subattribute(Rel, Relation).

is_wiki_relation(Page, Relation, Value) :- 
	wiki_relation(Page, Relation, Value).

is_wiki_relation(Page, Relation, Value) :-
	wiki_relation(Page, Rel, Value),
	is_wiki_subrelation(Rel, Relation).


