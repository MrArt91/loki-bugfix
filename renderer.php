<?php
/**
 * Loki Renderer Plugin
 *
 * @license	 GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @version	 1.0
 */
// must be run within Dokuwiki
if(!defined('DOKU_INC')) die();
//if(!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN',DOKU_INC.'lib/plugins/');

// we inherit from the XHTML renderer instead directly of the base renderer
require_once DOKU_INC.'inc/parser/xhtml.php';
require_once DOKU_INC.'inc/pageutils.php';
//require_once DOKU_INC.'lib/plugins/loki/loki_utl.php'
include_once DOKU_INC.'lib/plugins/loki/utl/loki_utl.php';
include_once DOKU_INC.'lib/plugins/loki/config.php';

/**
 * The Renderer
 */
class renderer_plugin_loki extends Doku_Renderer_xhtml {

	 /**
	  * return some info
	  */
  	function getInfo(){
		return array (
	 		'author' => 'GEIST Research Group',
			'email' => 'geist@agh.edu.pl',
			'date' => '2011-04-13',
			'name' => 'Loki Plugin (renderer component)',
			'desc' => 'Semantic plugin for DokuWiki',
			'url'	 => 'http://loki.ia.agh.edu.pl/download/loki.zip',
			);
	 }

	function canRender($format) {
				return ($format=='xhtml');
	}

	 //function cdata($text) {
	 //	 $this->doc .= $this->_xmlEntities($this->pl_filter($text));
	 //}

	 //function pl_filter($content) {
	////////////////////////////////////////////////////////////////////////
		  
	////////////////////////////////////////////////////////////////////////
	 //	 return $content;
	 //}


	function document_start() {

		$path=getID();
			$path_ex = explode(":",getID());
			$page=end($path_ex).".txt";
			
			$path= $path.".txt";
			$path = str_replace($page,"",$path);	
			$path=rtrim($path,":");
			
				
			
			$path=str_replace(":","/",$path);
			if(strlen($path)!=0)
			{
				$path .= "/";
			}
			
		
			$p=rtrim($conf[mediadir],"data/media");
			$fullpath=TEMP_DIR."/".$path;

			io_deleteFromFile($fullpath.$page,"/.*/",true); 

	}


	function document_end() {
			if(page_exists(getID())){
				/* for special pages */
				$path = getID();
				if (strcmp($path,"special:categories")==0){
					include_once(DOKU_INC.'lib/plugins/loki/utl/loki_utl_special.php');
					$utls=new LokiUtlSpecial;
					$categories = $utls->list_categories();
					if ($categories!=null)
					foreach($categories as $cat){
						$this->doc.= $this->internallink("special:category:".$cat,$cat,NULL,TRUE);
						$this->doc.="<br />";
					}						
				} elseif (strcmp($path,"special:relations")==0){
					include_once(DOKU_INC.'lib/plugins/loki/utl/loki_utl_special.php');
					$utls=new LokiUtlSpecial;
					$relations = $utls->list_relations();
					if ($relations!=null)
					foreach($relations as $rel){
						$this->doc.= $this->internallink("special:relation:".$rel,$rel,null,true);
						$this->doc.="<br />";
					}
				} elseif (strcmp($path,"special:attributes")==0){
					include_once(DOKU_INC.'lib/plugins/loki/utl/loki_utl_special.php');
					$utls=new LokiUtlSpecial;
					$attributes = $utls->list_attributes();
					if($attributes!=null)
					foreach($attributes as $att){
						$this->doc.= $this->internallink("special:attribute:".$att,$att,null,true);
						$this->doc.="<br />";
					}
				} elseif (!(strpos($path,"special:category:")===FALSE)) {
					include_once(DOKU_INC.'lib/plugins/loki/utl/loki_utl_special.php');
					$utls=new LokiUtlSpecial;
					$uses = $utls->list_category_uses(str_replace("special:category:","",$path));
					if( $uses != null){
						$this->doc.="<table border=\"1\"><tr><th>Usages</th></tr>";
						foreach ($uses as $u){
							$this->doc.="<tr><td>";
							$this->doc.= $this->internallink($u,null,null,true);	
							$this->doc.="</td></tr>";}
						$this->doc.="</table>";
					}
				} elseif (!(strpos($path,"special:relation:")===FALSE)) {
					include_once(DOKU_INC.'lib/plugins/loki/utl/loki_utl_special.php');
					$utls=new LokiUtlSpecial;
					$uses = $utls->list_relation_uses(str_replace("special:relation:","",$path));
					if( $uses != null){
						$this->doc.="<table border=\"1\"><tr><th>Page</th><th>".str_replace("special:relation:","",$path)."</th></tr>";
						foreach ($uses as $u){
							$this->doc.="<tr><td>";
							$this->doc.= $this->internallink($u[0],null,null,true);
							$this->doc.="</td><td>";
							$this->doc.= $this->internallink($u[1],null,null,true);	
							$this->doc.="</td></tr>";}
						$this->doc.="</table>";
					}
				} elseif (!(strpos($path,"special:attribute:")===FALSE)) {
					include_once(DOKU_INC.'lib/plugins/loki/utl/loki_utl_special.php');
					$utls=new LokiUtlSpecial;
					$uses = $utls->list_attribute_uses(str_replace("special:attribute:","",$path));
					if( $uses != null){
						$this->doc.="<table border=\"1\"><tr><th>Page</th><th>".str_replace("special:attribute:","",$path)."</th></tr>";
						foreach ($uses as $u){
							$this->doc.="<tr><td>";
							$this->doc.= $this->internallink($u[0],null,null,true);
							$this->doc.="</td><td>";
							$this->doc.=$u[1];	
							$this->doc.="</td></tr>";}
						$this->doc.="</table>";
					}
				}	
				global $ACT; 
				if($ACT == "show")
					$this->doc .= '<br><br><form class="button" method="get" action="'.wl($path).'">
	 							 <div align="right">
	 								<input type="submit" value="Export to RDF" class="button" />
	  						  		<input type="hidden" name="do" value="exportrdf" />								
		 							<input type="hidden" name="id" value="'.$path.'" />
	  							 </div>
								</form>';
		     }
			 $this->doc = preg_replace('#<p>\s*</p>#','',$this->doc);

		     if($this->slideopen){
		        $this->doc .= '</div>'.DOKU_LF; //close previous slide
		     }

	}
	 /**
	  * This is what creates new slides
	  *
	  * A new slide is started for each H2 header
	  */
	 function header($text, $level, $pos) {
			
			global $utl;
			if (!isset($utl))
				$utl = new LokiUtl;
			$path=getID();
				  
			$path_ex = explode(":",getID());
			$page=end($path_ex).".txt";
			
			$path= $path.".txt";
			$path = str_replace($page,"",$path);	
			$path=rtrim($path,":");
			
			$path=str_replace(":","/",$path);
			if(strlen(path)!=0)
			{
				$path .= "/";
			}
			
			$p=rtrim($conf[mediadir],"data/media");
			$fullpath=TEMP_DIR."/".$path;

			//$fullpath="/var/www/wiki/lib/plugins/tmp/".$path;
			//$this->doc .= "ID:".getID() ;  return;
			
			$utl->recursive_mkdir($fullpath);
				io_saveFile($fullpath.$page, "\nwiki_header('".$this->_xmlEntities($text)."',".$level.").",true);  //using dokuWiki framework

			  $hid = $this->_headerToLink($text,true);

		  //only add items within configured levels
		  $this->toc_additem($hid, $text, $level);

		  // write the header
		  $this->doc .= DOKU_LF.'<h'.$level.'><a name="'.$hid.'" id="'.$hid.'">';
		  $this->doc .= $this->_xmlEntities($text);
		  $this->doc .= "</a></h$level>".DOKU_LF;
	 }
	 
	 
	function internallink($id, $name = NULL, $search=NULL,$returnonly=false,$linktype='content') {
		  global $conf;
		  global $ID;
		  global $utl;
		  if(!isset($utl))
		  	$utl = new LokiUtl;
		  // default name is based on $id as given
		  $default = $this->_simpleTitle($id);
		$original_id = $id;
		  // now first resolve and clean up the $id
		  resolve_pageid(getNS($ID),$id,$exists);
	$orig_name = $name;
		  $name = $this->_getLinkTitle($name, $default, $isImage, $id, $linktype);
		  if ( !$isImage ) {
				if ( $exists ) {
					 $class='wikilink1';
				} else {
					 $class='wikilink2';
					 $link['rel']='nofollow';
				}
		  } else {
				$class='media';
		  }

		  //keep hash anchor
		  list($id,$hash) = explode('#',$id,2);
		  if(!empty($hash)) $hash = $this->_headerToLink($hash);

		  //prepare for formating
		  $link['target'] = $conf['target']['wiki'];
		  $link['style']  = '';
		  $link['pre']	 = '';
		  $link['suf']	 = '';
		  // highlight link to current page
		  if ($id == $ID) {
				$link['pre']	 = '<span class="curid">';
				$link['suf']	 = '</span>';
		  }
		  $link['more']	= '';
		  $link['class']  = $class;
		  $link['url']	 = wl($id);
		  $link['name']	= $name;
		  $link['title']  = $id;
		  //add search string
			if($search){
				($conf['userewrite']) ? $link['url'].='?' : $link['url'].='&amp;';
				if(is_array($search)){
					 $search = array_map('rawurlencode',$search);
					 $link['url'] .= 's[]='.join('&amp;s[]=',$search);
				}else{
					 $link['url'] .= 's='.rawurlencode($search);
				}
		  }

		  //keep hash
		  if($hash) $link['url'].='#'.$hash;

		  //output formatted
		  if($returnonly)
			  {
					return $this->_formatLink($link);
			  }
				
				$path=getID();
			$pageid = $path;
				  
			$path_ex = explode(":",getID());
			$page=end($path_ex).".txt";
			
			$path= $path.".txt";
			$path = str_replace($page,"",$path);	
			$path=rtrim($path,":");
			
			$path=str_replace(":","/",$path);
			if(strlen(path)!=0)
			{
				$path .= "/";
			}		

			$p=rtrim($conf[mediadir],"data/media");
			$fullpath=TEMP_DIR."/".$path;

			//$fullpath="/var/www/wiki/lib/plugins/tmp/".$path;
			//$this->doc .= "ID:".getID() ;  return;
			$utl->recursive_mkdir($fullpath);
				/////
				
			//semantic mediaWiki syntax support:
			$is_category = stripos($original_id,"category:");
			$is_relation = strpos($original_id,"::");
			$is_attribute = strpos($original_id,":=");
			$is_subproperty = stripos($original_id,"subproperty of");
			$is_subrelation = stripos($original_id,"subrelation of");
			$is_subattribute = stripos($original_id,"subattribute of");
			

			if((!($is_subrelation === false) || !($is_subproperty ===false)) && !(stripos($pageid,"special:relation:")===false) ) 
			{	
				$pagename = str_replace("special:relation:","",$pageid);
				$arguments =  explode("::",$original_id);
				io_saveFile($fullpath.$page, "\nwiki_subrelation('".$pagename."','".trim($arguments[1])."').",true);  //using dokuWiki framework	
				if(strcmp($orig_name,' ')) $this->doc .= $this->internallink("special:relation:".$arguments[1], $name, NULL,true);
			}
			elseif((!($is_subattribute === false) || !($is_subproperty ===false)) && !(stripos($pageid,"special:attribute:")===false) ) 
			{
				
				$pagename = str_replace("special:attribute:","",$pageid);
				$arguments =  preg_split("/[(::)(:=)]/",$original_id,-1,PREG_SPLIT_NO_EMPTY);
				io_saveFile($fullpath.$page, "\nwiki_subattribute('".$pagename."','".trim($arguments[1])."').",true);  //using dokuWiki framework	
				if(strcmp($orig_name,' ')) $this->doc .= $this->internallink("special:attribute:".trim($arguments[1]), ltrim($name,"="), NULL,true);
			}						
			elseif(!($is_category === false) && stripos($original_id,"special:category")===false) 
			{
				$arguments = substr($original_id,strpos($original_id,":")+1);
				if (!(strpos($pageid,"special:category:")===false) && strpos($pageid,"special:category:")==0){ //dla stron specjalnych kategorii
					$pagename = str_replace("special:category:","",$pageid);	
					io_saveFile($fullpath.$page, "\nwiki_subcategory('".$pagename."','".trim($arguments)."').",true);  //using dokuWiki framework					
				} else { //dla pozostalych stron
					io_saveFile($fullpath.$page, "\nwiki_category('".getID()."','".trim($arguments)."').",true);  //using dokuWiki framework
				}
				if(strcmp($orig_name,' ')) $this->doc .= $this->internallink("special:category:".trim($arguments), $name, NULL,true);
			}
			elseif (!($is_relation === false)) 
			{
				$arguments =  explode("::",$original_id);	
				$args = count($arguments);
				for ($i=0; $i<=$args-2; $i+=1)
					io_saveFile($fullpath.$page, "\nwiki_relation('".getID()."','".trim($arguments[$i])."','".trim($arguments[$args-1])."').",true);  //using dokuWiki framework	
				if(strcmp($orig_name,' ')) $this->doc .= $this->internallink(trim($arguments[$args-1]), $name, NULL,true);
			}	 
			elseif (!($is_attribute === false)) 
			{
				$arguments =  explode(":=",$original_id);
				$args = count($arguments);
				for ($i=0; $i<=$args-2; $i+=1)
					io_saveFile($fullpath.$page, "\nwiki_attribute('".getID()."','".trim($arguments[$i])."','".trim($arguments[$args-1])."').",true);  //using dokuWiki framework
				if(strcmp($orig_name,' ')) $this->doc .= ltrim($name,"=");
			} 
			else 
			{
				io_saveFile($fullpath.$page, "\nwiki_internallink('".$link['name']."','".$link['url']."').",true);  //using dokuWiki framework
				$this->doc .= $this->_formatLink($link);
			} 			
			  }

	  function externallink($url, $name = NULL) {
		  global $conf;
		  global $utl;
		  if(!isset($utl))
		  	$utl = new LokiUtl;

		  $name = $this->_getLinkTitle($name, $url, $isImage);

		  if ( !$isImage ) {
				$class='urlextern';
		  } else {
				$class='media';
		  }

		  //prepare for formating
		  $link['target'] = $conf['target']['extern'];
		  $link['style']  = '';
		  $link['pre']	 = '';
		  $link['suf']	 = '';
		  $link['more']	= '';
		  $link['class']  = $class;
		  $link['url']	 = $url;

		  $link['name']	= $name;
		  $link['title']  = $this->_xmlEntities($url);
		  if($conf['relnofollow']) $link['more'] .= ' rel="nofollow"';

	 
		  $this->doc .= $this->_formatLink($link);
				  
				$path=getID();
				  
			$path_ex = explode(":",getID());
			$page=end($path_ex).".txt";
			
			$path= $path.".txt";
			$path = str_replace($page,"",$path);	
			$path=rtrim($path,":");
			
			$path=str_replace(":","/",$path);
			if(strlen(path)!=0)
			{
				$path .= "/";
			}
			
			$p=rtrim($conf[mediadir],"data/media");
			$fullpath=TEMP_DIR."/".$path;

			//$fullpath="/var/www/wiki/lib/plugins/tmp/".$path;
			//$this->doc .= "ID:".getID() ;  return;
			$utl->recursive_mkdir($fullpath);
			
			io_saveFile($fullpath.$page, "\nwiki_externallink('".$link['name']."','".$link['url']."').",true);  //using dokuWiki framework

	  }
		  
	  ////////////
		function internalmedia ($src, $title=NULL, $align=NULL, $width=NULL,
										 $height=NULL, $cache=NULL, $linking=NULL) 
 			{
			 
 			$is_ask = strpos($src,"#ask:");
 			
 			if(!($is_ask === false))
 			{		
 				global $ID;
				$asksrc=$src."|".$title;
				include_once(DOKU_INC.'lib/plugins/loki/utl/loki_utl_ask.php');
				$utla= new LokiUtlAsk;
				global $utl;
				if(!isset($utl))
					$utl = new LokiUtl;
				$result = $utla->process_ask($asksrc);
				$options = $result["options"];
				$properties=$result["properties"];
				$goal=$result["goal"];
				$rtab = $result["result"];
				$rtext = "";
				$number_of_results = count($rtab);
				
				//displaying results
				if ($options["format"] == "debug")
				{
					$rtext .= "</p><div style=\"border: 1px dotted black; background: #AEDFFF; padding: 15px;\">";
					$rtext .= "<strong>Debug information for Loki Ask query</strong><br />";
					$rtext .= "Query source code:<br />".$asksrc."<br />Main Prolog goal:<br />";
					$rtext .= str_replace("), write(X),write('%%'),fail.","",substr($goal,1))."<br />Properties to display:<br />";
					foreach ($properties as $p)
						$rtext .=$p["property"].", ";
					$rtext = rtrim($rtext,", ");
					$rtext .= "<br />Query options:<br />";
					foreach ($options as $okey=>$ovalue)
						$rtext .=$okey."=\"".$ovalue."\", ";
					$rtext = rtrim($rtext,", ");
					$rtext .= "<br />Number of results: ".($rtab==null?"0":$number_of_results).".";
			/*		$rtext .="Results:<br />";
  					foreach ($rtab as $key=>$value) {
 					$rtext.= "key $key <br />";
					foreach ($value as $iKey => $iValue) {
					$rtext.=" ---> $iKey - $iValue<br />";}}
		*/			$rtext .= "</div><p>";
					
					$this->doc.=$rtext;
				}
				elseif ($options["format"] == "count")
				{
					$rtext = str_replace("_"," ",$options["intro"])
						.($rtab==null?"0":$rtab)
						.str_replace("_"," ",$options["outro"]);
					$this->doc .= $rtext;
				}
				elseif ($rtab==NULL )
				{
					$rtext .= str_replace("_"," ",$options["intro"])
						.str_replace("_"," ",$options["default"])
						.str_replace("_"," ",$options["outro"]);
					$this->doc .= $rtext;
				}
				else
				{
					$is_tabular = in_array($options["format"], array("table","broadtable","csv"));
					$is_list = in_array($options["format"], array("ul","ol","list"));
					$fo = $utla -> prepare_format($options);
					
					$this->doc .= str_replace("_"," ",$options["intro"]);
					$rtext .= $fo["before_all"];
					if ($is_tabular && $options["headers"] == "show")
					{
						$rtext .= $fo["before_row"];
						if($options["mainlabel"] != "-")
							$rtext .= $fo["before_header"].$options["mainlabel"].$fo["after_header"];
						for ($i=1; $i< count($rtab[0]); $i++)
						{
							$rtext .= $fo["before_header"];
							if ($options["format"] == "csv")
								$rtext .= $properties[$i]["display"];
							elseif ($properties[$i]["property"] == "category")
							 	$rtext .= ($properties[$i]["argument"] == "") ? "Category" : $this->internallink("special:category:".$properties[$i]["argument"],$properties[$i]["display"],null, true);
							elseif ($utl->is_relation($properties[$i]["property"]))
								$rtext .= $this->internallink("special:relation:".$properties[$i]["property"],$properties[$i]["display"],null, true) ;
							else					
								$rtext .= $this->internallink("special:attribute:".$properties[$i]["property"],$properties[$i]["display"],null, true) ;
							$rtext .= $fo["after_header"];
						}
						if ($options["format"]=="csv")
							$rtext = substr($rtext, 0, strlen($rtext)-strlen($options["sep"]));
						$rtext .= $fo["after_row"];
					}
					$row_start = (int)$options["offset"] > 0 ? (int)$options["offset"] : 1;
					$row_finish = (int)$options["limit"] >= 0 ? $row_start-1 + (int)$options["limit"] : $number_of_results;
					
					for ($row=$row_start-1; $row<$row_finish && $row<$number_of_results; $row++)
					{
						$rtext .= $fo["before_row"];
						if($options["mainlabel"] != "-")
						{
							if ($is_tabular) $rtext .= $fo["before_element"];
							$rtext .= strpos($properties[0]["link"],"#")===FALSE && $options["format"]!="csv" ? 
									$this->internallink($rtab[$row][0],null,null, true) : $rtab[$row][0];
							if ($is_tabular) $rtext .= $fo["after_element"];
						}
						if (count($rtab[0])>1) $rtext .= $fo["before_elements"];
						for ($i=1; $i< count($rtab[0]) ; $i++)
						{
							if ($is_list && $options["headers"] =="show")
							{
								$rtext .= $fo["before_header"];
								if ($properties[$i]["property"] == "category")
								 	$rtext .= ($properties[$i]["argument"] == "") ? "Category" : $this->internallink("special:category:".$properties[$i]["argument"],$properties[$i]["display"],null, true);
								elseif ($utl->is_relation($properties[$i]["property"]))
									$rtext .= $this->internallink("special:relation:".$properties[$i]["property"],$properties[$i]["display"],null, true) ;
								else					
									$rtext .= $this->internallink("special:attribute:".$properties[$i]["property"],$properties[$i]["display"],null, true) ;
								$rtext .= $fo["after_header"];								
							}
							$rtext .= $fo["before_element"];
							if ( strpos($properties[$i]["link"],"#")===FALSE && $options["format"] != "csv")
							{
								$ress = explode("%%",$rtab[$row][$i]);
								foreach ($ress as $rr)
								{
									if ($properties[$i]["property"] == "category")
										$rtext .= $this->internallink("special:category:".$rr,null, null, true);
									else
										$rtext .= $rr==""? "" : $this->internallink($rr,null,null, true) ;
									$rtext .= $is_tabular ? "<br />" : ", ";
								}
							}
							elseif ($options["format"] == "csv")
								$rtext .= str_replace("%%","\r\n",$rtab[$row][$i]);
							else
							{
								$rtext .= $is_tabular ? str_replace("%%","<br />",$rtab[$row][$i]) : str_replace("%%",", ",$rtab[$row][$i]);
							}
							
							$rtext .= $fo["after_element"];
						}
						if (count($rtab[0])>1) $rtext .= $fo["after_elements"];
						if ($options["format"]=="csv")
							$rtext = substr($rtext, 0, strlen($rtext)-strlen($options["sep"]));
						$rtext .= $fo["after_row"];						
					}
					
					$rtext .= $fo["after_all"];
					
					if( $options["format"] == "csv")
					{
						$filename = $conf[mediadir]."/".str_replace(":","/",getNS($ID))."/".noNS($ID).'_'.date('ymd');
						global $conf; $n=1; $found = false;
						$utl->recursive_mkdir($conf[mediadir]."/".str_replace(":","/",getNS($ID)));
						if (!file_exists($filename.".csv"))
						{
							io_saveFile($conf[mediadir]."/".str_replace(":","/",getNS($ID))."/".noNS($ID).'_'.date('ymd').".csv", $rtext,false);
							$this->doc .= $this->internalmedia(noNS($ID)."_".date("ymd").".csv","generated CSV file");	
							$found = true;
						}else
							if (file_get_contents($filename.".csv") == $rtext)	
							{
								$this->doc .= $this->internalmedia(noNS($ID)."_".date("ymd").".csv","generated CSV file");
								$found=true;
							} else 
							{
								while(!found && file_exists($filename."_$n.csv"))
									if (file_get_contents($filename."_$n.csv") == $rtext)
									{	
										$this->doc .= $this->internalmedia(noNS($ID)."_".date("ymd")."_$n.csv","generated CSV file");
										$found = true;
									} else $n++;
							}
						if (!$found)	
						{	
							io_saveFile($conf[mediadir]."/".str_replace(":","/",getNS($ID))."/".noNS($ID).'_'.date('ymd')."_$n.csv", $rtext,false);
							$this->doc .= "<a href=\"".ml(($ID)."_".date("ymd")."_$n.csv",'')."\">generated CSV file</a>";
						}						
							

						
					}
					else	$this->doc .= $rtext;
					$this->doc .= str_replace("_"," ",$options["outro"]);
				}
				
			/*	$this->doc.=$goal."<br />";
				foreach($properties as $p) $this->doc.=$p["property"]."-".$p["display"]."- ".$p["option"].";;  ";
				$this->doc.="<br />";
				foreach($options as $key=>$value) $this->doc.=$key." => ".$value.";;	";
	 			/*$goal = "";
	 			
	 			$pattern = '/\[\[([^\]])*]\]/';
	 			preg_match_all($pattern,$src, $matches, PREG_SET_ORDER);
	 			foreach ($matches as $val)
				{
					$curr = $val[0];
					$curr = ltrim($curr, "[");
					$curr = rtrim($curr, "]");
					$is_category = strpos($curr,"category");
					$is_relation = strpos($curr,"::");
					$is_attribute = strpos($curr,":=");
								  
					if(!($is_category === false)) 
					{
						$arguments =  substr($curr,strpos($curr,":")+1);
						$goal .= "wiki_category(X,'".$arguments[1]."'),";
					}
					elseif (!($is_relation === false)) 
					{
						$arguments =  explode("::", $curr);
						$goal .= "wiki_relation(X,'".$arguments[0]."','".$arguments[1]."'),";
					}	 
					elseif (!($is_attribute === false)) 
					{
						$arguments =  explode(":=", $curr);
						$goal .= "wiki_attribute(X,'".$arguments[0]."','".$arguments[1]."'),";
					} 
				
				}
				
				$goal .= "write(X),write('%'),fail.";
	 			
				$scope = "*";
				TEMP_DIR = getcwd()."/"."lib/plugins/tmp/loki";	
				$hash = md5(serialize($goal.time()));
				//exec('touch '.TEMP_DIR."dokuwiki.code".$hash);
				//exec('touch '.TEMP_DIR."dokuwiki.loki".$hash);
				
				//exec('chmod 777 '.TEMP_DIR."dokuwiki.code".$hash);
				//exec('chmod 777 '.TEMP_DIR."dokuwiki.loki".$hash);
				
				$scope = TEMP_DIR."/".$scope;

				exec('echo ":- style_check(-discontiguous)." > '.TEMP_DIR.'dokuwiki.code'.$hash);
				exec('grep ".*" -rh  $(grep -rl ".*" '. TEMP_DIR.' | grep "'.$scope.'")  >> '.TEMP_DIR.'dokuwiki.code'.$hash);
				exec('LANG=pl_PL.utf-8; /usr/bin/swipl -q -s '.TEMP_DIR.'dokuwiki.code'.$hash.' -g "'.$goal.'" -t halt > '.TEMP_DIR.'dokuwiki.loki'.$hash.$msgerrr) ;
				$result =  file_get_contents(TEMP_DIR."dokuwiki.loki".$hash);	
				
				//$this->doc .= file_get_contents(TEMP_DIR."dokuwiki.loki");			
				
				  //exec("rm -f ".TEMP_DIR."dokuwiki.code".$hash);
					//exec("rm -f ".TEMP_DIR."dokuwiki.loki".$hash);
				
					@unlink(TEMP_DIR."dokuwiki.code".$hash);
					@unlink(TEMP_DIR."dokuwiki.loki".$hash);
					
				if (strpos($result,"%") === false)
				{
				 	$this->doc .= "";
				 	return;
				}
				
				$pages =  explode("%", rtrim($result,"%"));
				$retval = "";
				foreach ($pages as $item)
				{
					$retval .=  $this->internallink($item, NULL, NULL,true)." ";
					//$retval .= $item.;
				}
				
				  //exec("rm -f ".TEMP_DIR."dokuwiki.code".$hash);
				//	exec("rm -f ".TEMP_DIR."dokuwiki.lokidokuwiki.loki".$hash);
	 			

				$this->doc .= $retval;*/
 			}
 			else
 			{
 			// DO NOT CHANGE !!!!!!!!!!!!!!!!!!!!!!!!!
 			// from DokuWiki file inc/parser/xhtml.php 
		 		global $ID;
				  resolve_mediaid(getNS($ID),$src, $exists);
		
				  $noLink = false;
				  $render = ($linking == 'linkonly') ? false : true;
				  $link = $this->_getMediaLinkConf($src, $title, $align, $width, $height, $cache, $render);
		
				  list($ext,$mime) = mimetype($src);
				  if(substr($mime,0,5) == 'image' && $render){
						$link['url'] = ml($src,array('id'=>$ID,'cache'=>$cache),($linking=='direct'));
				  }elseif($mime == 'application/x-shockwave-flash'){
						// don't link flash movies
						$noLink = true;
				  }else{
						// add file icons
						$class = preg_replace('/[^_\-a-z0-9]+/i','_',$ext);
						$link['class'] .= ' mediafile mf_'.$class;
						$link['url'] = ml($src,array('id'=>$ID,'cache'=>$cache),true);
				  }
		
				  //output formatted
				  if ($linking == 'nolink' || $noLink) $this->doc .= $link['name'];
				  else $this->doc .= $this->_formatLink($link);
			 // DO NOT CHANGE !!!!!!!!!!!!!!!!!!!!!!!!!
 			// from DokuWiki file inc/parser/xhtml.php 
 			}
	  }
	

}

?>
