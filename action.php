<?php
 /**
 * Loki Action Plugin - A semantic plugin for DokuWiki.
 *
 * @license		GPL 2 (http://www.gnu.org/licenses/gpl.html)
 * @author		GEIST Research Group <geist@agh.edu.pl>
 */

// must be run within Dokuwiki
if (!defined('DOKU_INC')) die();
if (!defined('DOKU_PLUGIN')) define('DOKU_PLUGIN', DOKU_INC . 'lib/plugins/');

require_once (DOKU_PLUGIN . 'action.php');
 
class action_plugin_loki extends DokuWiki_Action_Plugin
{
	/** Constructor */
	function action_plugin_loki(){
	}

	function getInfo(){
		return array (
	 		'author' => 'GEIST Research Group',
			'email' => 'geist@agh.edu.pl',
			'date' => '2011-04-13',
			'name' => 'Loki Plugin (action component)',
			'desc' => 'Semantic plugin for DokuWiki',
			'url'	 => 'http://loki.ia.agh.edu.pl/download/loki.zip',
			);
		}

		/**
		 * Register the events
		 */
		function register(&$controller)
		{
				$controller->register_hook('ACTION_ACT_PREPROCESS', 'BEFORE', $this, 'exportrdf',array());
				$controller->register_hook('IO_WIKIPAGE_WRITE', 'BEFORE', $this, 'delete_loki_page',array());
		}

		function delete_loki_page(&$event, $param)
        {
            $namespace = $event->data[1];
            $page_name = $event->data[2];
            $revision = $event->data[3];
            $content = $event->data[0][1];

            if (empty($revision) && empty($content))
            {
                $temp_dir = getcwd()."/"."lib/plugins/loki/tmp/loki";
                $file_to_delete = $temp_dir.'/'.str_replace(":", "/", $namespace).'/'.$page_name.'.txt';

                if (file_exists($file_to_delete))
                {
                    unlink($file_to_delete);
                }

                // remove empty directory
                $cmd = 'find "'.$temp_dir.'" -type d -empty -exec rmdir -p {} \; 2>/dev/null';
                shell_exec($cmd);
            }
        }

		function exportrdf(&$event, $param)
		{
			global $ACT;
			global $ID;
			global $conf;
			include_once(DOKU_INC."/lib/plugins/loki/utl/loki_utl.php");
			global $utl;
			if (!isset($utl))
				$utl = new LokiUtl; 
					
			if (!(strpos($ID,"special:uriresolve:")===false) && $ACT=='show')
			{
				$uri = $_SERVER['REQUEST_URI'];
				$uri = str_replace("special:uriresolve:","",substr($uri,strpos($uri,"special:uriresolve")));
				$iuri = str_replace('special:uriresolve:',"",$ID);
				if(stripos($uri,'http://')===false)
					header("Location: ".wl($iuri,'',TRUE));
				else 
					header("Location: ".$uri);
			}
			
			if (( $ACT == 'exportrdf' ) || ( $ACT == 'export_rdf' )) {
			
				// check user's rights
				if ( auth_quickaclcheck($ID) < AUTH_READ ) {
					return false;
				}	
				include_once(DOKU_INC."/lib/plugins/loki/utl/loki_utl_rdf.php");
				$utlr = new LokiUtlRdf;
				$rdfcontent = $utlr->export_rdf($ID,$conf['plugin']['loki']['rdfexport_maxlevels']);
				
				$found = false;
				$utl->recursive_mkdir($conf[mediadir]."/".str_replace(":","/",getNS($ID)));
				if (!file_exists($conf[mediadir]."/".str_replace(":","/",getNS($ID))."/".noNS($ID).".rdf.xml"))
				{
					io_saveFile($conf[mediadir]."/".str_replace(":","/",getNS($ID))."/".noNS($ID).".rdf.xml",$rdfcontent,false);
					$link = $ID.".rdf.xml";
					$found = true;
				} else
				{
					if(file_get_contents($conf[mediadir]."/".str_replace(":","/",getNS($ID))."/".noNS($ID).".rdf.xml") == $rdfcontent)
					{
						$link = $ID.".rdf.xml";
						$found = true;
					}
					else
					{
						$i=1;
						while(file_exists($conf[mediadir]."/".str_replace(":","/",getNS($ID))."/".noNS($ID)."_$i.rdf.xml") && $found==false)
						{
							if(file_get_contents($conf[mediadir]."/".str_replace(":","/",getNS($ID))."/".noNS($ID)."_$i.rdf.xml") == $rdfcontent)
							{
								$link = $ID."_$i.rdf.xml";
								$found = true;
							}	
							$i++;												
						}
						if ($found==false)
						{
							io_saveFile($conf[mediadir]."/".str_replace(":","/",getNS($ID))."/".noNS($ID)."_$i.rdf.xml",$rdfcontent,false);
							$link = $ID."_$i.rdf.xml";
							$found = true;						
						}				
					}
				}
	
				print( "<a href=\"".ml($link,'')."\">Generated RDF file</a>");
				print( "<br><br><br><a href=\"".wl(getID())."\">back</a>");
				
				die();
			}
		}
		

}
?>
