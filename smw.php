<?php
/*************************************************************************************
 * smw.php
 * -------------
 * Author: GEIST Research Group
 * Release Version: 1.0.0
 * Date Started: 2010/07/07
 *
 * Semantic MediaWiki (as used in Loki) language file for GeSHi.
 *
 * CHANGES
 * -------
 * 2011/04/14 (1.0.0)
 *   -  plwiki changed to loki
 * 2010/07/07 (1.0.0)
 *   -  First Release
 *
 * TODO (updated 2004/07/14)
 * -------------------------
 * *
 *
 ************************************************************************************/

$language_data = array (
    'LANG_NAME' => 'Semantic MediaWiki',
    'COMMENT_SINGLE' => array(),
    'COMMENT_MULTI' => array(),
    'CASE_KEYWORDS' => GESHI_CAPS_NO_CHANGE,
    'QUOTEMARKS' => array(),
    'ESCAPE_CHAR' => '',
    'KEYWORDS' => array( 
	1 => array('#ask'),
        2 => array('category:')
	),
    'SYMBOLS' => array(
	0 => array('{{','}}'),
        1 => array('[[',']]'),
	3 => array(':','=','#')
	),
    'CASE_SENSITIVE' => array(
        GESHI_COMMENTS => false,
	1 => false,
	2 => false
        ),
    'STYLES' => array(
        'KEYWORDS' => array(
		1 => 'color: #cc0000;',
		2 => 'color: #cccc00;'
		),
        'COMMENTS' => array(),
        'ESCAPE_CHAR' => array(),
        'BRACKETS' => array(),
        'STRINGS' => array(),
        'NUMBERS' => array(),
        'METHODS' => array(),
        'SYMBOLS' => array(
		0 => 'color: #ffcc55;',
		1 => 'color: #55ccff;'
		),
        'SCRIPT' => array(
		0 => '',
		1 => ''		
		),
        'REGEXPS' => array(
		1 => 'color: #00cccc;',
		2 => 'color: #cc00cc;'
		)
        ),
    'URLS' => array(),
    'OOLANG' => false,
    'OBJECT_SPLITTERS' => array(),
    'REGEXPS' => array(
		1 => '[A-Za-z0-9_:\b\*]*[A-Za-z0-9_\b\*](::)',
		2 => '[A-Za-z0-9_:\b\*]*[A-Za-z0-9_\b\*](:=)'
	),
    'STRICT_MODE_APPLIES' => GESHI_MAYBE,
    'SCRIPT_DELIMITERS' => array(
	0 => array(
            '[[' => ']]'
            ),
        1 => array(
            '{{' => '}}'
            )	
	),
    'HIGHLIGHT_STRICT_BLOCK' => array(
    	    0 => true,
    	    1 => true
	),
    'TAB_WIDTH' => 4
    
);


?>
