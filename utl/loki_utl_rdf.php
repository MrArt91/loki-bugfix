<?php

include_once DOKU_INC.'lib/plugins/loki/utl/loki_utl.php';

/**
 * Utility class for Loki - contains functions used for RDF export
 *
 * @author GEIST Research Group
 */
class LokiUtlRdf  {  
 
	// Predefined namespaces
	var $namespaces = array(
		'rdf' => 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
		'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
		'owl' => 'http://www.w3.org/2002/07/owl#',
	);
 
	// main template for the RDF/XML document
 	var $template = array(
		"intro" => "<?xml version=\"1.0\"?>\n\n<rdf:RDF",
		"namespaces" => "\n\t\txmlns:%%NS%%=\"%%NSURI%%\"",  //%%NS%%, %%NSURI%%
		"intro2" => ">\n\n\t<!-- exported page data -->",
		"data" => array(
					"intro" => "\n\t<rdf:Description\n\t rdf:about=\"%%PAGEURL%%\">",		//%%PAGEURL%%
					"label" => "\n\t\t<rdfs:label>%%LABEL%%</rdfs:label>",	//%%LABEL%%
					"category" => "\n\t\t<rdf:type rdf:resource=\"%%CATURL%%\"/>", //%%CATURL%%
					"relation" => "\n\t\t<lokirel:%%RELNAME%% rdf:resource=\"%%VALUE%%\"/>", 	//%%RELNAME%%,%%VALUE%%
					"attribute" => "\n\t\t<lokiatt:%%ATTNAME%% rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">%%VALUE%%</lokiatt:%%ATTNAME%%>", //%%ATTNAME%%,%%VALUE%%
					"outro" => "\n\t\t<rdfs:isDefinedBy rdf:resource=\"%%EXPORTURL%%\"/>\n\t</rdf:Description>\n"	 //%%EXPORTURL%%
					),
		"adds" => "\n\t<!-- related pages data -->",
		"onto" => "\n\t<!-- related ontology data -->",	
		"category" => array(
					"intro" => "\n\t<owl:Class\n\t rdf:about=\"%%PAGEURL%%\">",		//%%PAGEURL%%
					"label" => "\n\t\t<rdfs:label>%%LABEL%%</rdfs:label>",	//%%LABEL%%
					"subcategory" => "\n\t\t<rdfs:subClassOf rdf:resource=\"%%CATURL%%\"/>", //%%CATURL%%
					"outro" => "\n\t\t<rdfs:isDefinedBy rdf:resource=\"%%EXPORTURL%%\"/>\n\t</owl:Class>\n"	 //%%EXPORTURL%%		
					),
		"attribute" => array(
					"intro" => "\n\t<owl:DatatypeProperty\n\t rdf:about=\"%%PAGEURL%%\">",		//%%PAGEURL%%
					"label" => "\n\t\t<rdfs:label>%%LABEL%%</rdfs:label>",	//%%LABEL%%
					"subattribute" => "\n\t\t<rdfs:subPropertyOf rdf:resource=\"%%ATTURL%%\"/>", //%%ATTURL%%
					"outro" => "\n\t\t<rdfs:isDefinedBy rdf:resource=\"%%EXPORTURL%%\"/>\n\t</owl:DatatypeProperty>\n"	 //%%EXPORTURL%%		
					),
		"relation" => array(
					"intro" => "\n\t<owl:ObjectProperty\n\t rdf:about=\"%%PAGEURL%%\">",		//%%PAGEURL%%
					"label" => "\n\t\t<rdfs:label>%%LABEL%%</rdfs:label>",	//%%LABEL%%
					"subrelation" => "\n\t\t<rdfs:subPropertyOf rdf:resource=\"%%RELURL%%\"/>", //%%RELURL%%
					"outro" => "\n\t\t<rdfs:isDefinedBy rdf:resource=\"%%EXPORTURL%%\"/>\n\t</owl:ObjectProperty>\n"	 //%%EXPORTURL%%		
					),						
		"outro" => "\n</rdf:RDF>\n"
	);
 	
 	//pages for export
 	var $pages = null;
 	
 	// ontology elements for export
 	var $ontology = array("c"=>null, "a"=>null, "r"=>null);
 	
 	
 	/** function for exporting a single page data */
 	private function export_page($id, $level, $maxlevels)
 	{
		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl; 		
 		$url = wl($id,'',true);
 		$exporturl = wl($id, array('do'=>'exportrdf'),true);
 		$uriresolver=wl('special:uriresolve','',true);
 		$ns = getNS($id);
 		$label = noNS($id);
 		$result.= str_replace("%%PAGEURL%%",$uriresolver.":".$id,$this->template["data"]["intro"]);
 		$result.=str_replace("%%LABEL%%",$label,$this->template["data"]["label"]);
 		$goal = "(wiki_category('$id',Y),write('c%%'),write(Y),write('%%%'),fail);(wiki_attribute('$id',Y,Z),write('a%%'),write(Y),write('%%'),write(Z),write('%%%'),fail);(wiki_relation('$id',Y,Z),write('r%%'),write(Y),write('%%'),write(Z),write('%%%'),fail).";
 		$res = $utl->call_prolog($goal);
 		if(!(strpos($res,"%%%")===false))
 		{
 			$res= rtrim($res,"%");
 			$elems = explode("%%%",$res);
 			foreach ($elems as $elem)
 			{	
 				$el = explode("%%",$elem);
 				if ($el[0]=='c')
 				{
 					$el[1] = cleanID("special:category:".$el[1]);
 					if (!isset($this->ontology["c"]) || !in_array($el[1],$this->ontology["c"]))
 						$this->ontology["c"][] = $el[1];
 					$result.= str_replace("%%CATURL%%",$uriresolver.":".$el[1],$this->template["data"]["category"]);
 				}
 				elseif ($el[0]=='a')
 				{
  					$el[1] = cleanID("special:attribute:".$el[1]);
 					if (!isset($this->ontology["a"]) || !in_array($el[1],$this->ontology["a"]))
 						$this->ontology["a"][] = $el[1];
 					$lab = noNS($el[1]);
 					$result.= str_replace(array("%%ATTNAME%%","%%VALUE%%"),array($lab,$el[2]),$this->template["data"]["attribute"]);				
 				}
 				elseif ($el[0]=='r')
 				{
 					$el[1] = cleanID($el[1]);
 					$el[1] = "special:relation:".$el[1];
 					if (!isset($this->ontology["r"]) || !in_array($el[1],$this->ontology["r"]))
 						$this->ontology["r"][] = $el[1];
 					$lab = noNS($el[1]);
 					$el[2] = cleanID($el[2]);
 					if (getNS($el[2])===false) 
 						$el[2] = $ns.":".$el[2];
 					if ($level<$maxlevels)
 					{
 						$done=false;
 						for ($i = 1; $i<=$maxlevels; $i++)
 						{
 							if(isset($this->pages[$i+1]) && in_array($el[2],$this->pages[$i+1]))
 								$done=true;
 						}
 						if ($done==false)
 							$this->pages[$level+1][]=$el[2];
 					}
 					$result.= str_replace(array("%%RELNAME%%","%%VALUE%%"),array($lab,$uriresolver.":".$el[2]),$this->template["data"]["relation"]);		
 				
 				}
 			}		
 		}
 		$result.= str_replace("%%EXPORTURL%%",$exporturl,$this->template["data"]["outro"]);
 		return $result;
 	}
 	
 	/** function for exporting a single ontology item data */
 	private function export_onto($id,$type)
 	{
 		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl;
 		$url = wl($id,'',true);
 		$exporturl = wl($id, array('do'=>'exportrdf'),true);
 		$uriresolver=wl('special:uriresolve','',true);
 		$ns = getNS($id);
 		$label = noNS($id);
 		switch($type)
 		{
 			case "c":
 				$result.= str_replace("%%PAGEURL%%",$uriresolver.":".$id,$this->template["category"]["intro"]);
 				$result.= str_replace("%%LABEL%%",$label,$this->template["category"]["label"]);
 				$goal = "wiki_subcategory('$label',X),write(X),write('%%'),fail.";
 				$res = $utl->call_prolog($goal);
 				if (!(strpos($res,"%%")===false))
 				{
 					$res = rtrim($res,"%");
 					$elems = explode("%%",$res);
 					foreach($elems as $el)
 					{
 						$el = str_replace("special:category:","",$el);
 						$el = cleanID($el);
 						
 						$el = "special:category:".$el;
 						if (!isset($this->ontology["c"]) || !in_array($el,$this->ontology["c"]))
 							$this->ontology["c"][]=$el;
 						$result.= str_replace("%%CATURL%%",$uriresolver.":".$el,$this->template["category"]["subcategory"]);
 					}			
 				}	
 				$result .= str_replace("%%EXPORTURL%%",$exporturl,$this->template["category"]["outro"]);
 				break;	
 			case "r":
 			 	$result.= str_replace("%%PAGEURL%%",$uriresolver.":".$id,$this->template["relation"]["intro"]);
 				$result.= str_replace("%%LABEL%%",$label,$this->template["relation"]["label"]);
 				$goal = "wiki_subrelation('$label',X),write(X),write('%%'),fail.";
 				$res = $utl->call_prolog($goal);
 				if (!(strpos($res,"%%")===false))
 				{
 					$res = rtrim($res,"%");
 					$elems = explode("%%",$res);
 					foreach($elems as $el)
 					{
 						$el = str_replace("special:relation:","",$el);
 						$el = cleanID($el);
 						$el = "special:relation:".$el;
 						if (!isset($this->ontology["r"]) || !in_array($el,$this->ontology["r"]))
 							$this->ontology["r"][]=$el;
 						$result.= str_replace("%%RELURL%%",$uriresolver.":".$el,$this->template["relation"]["subrelation"]);
 					}			
 				}
 				$result .= str_replace("%%EXPORTURL%%",$exporturl,$this->template["relation"]["outro"]);
 				break;
 			case "a":
 				$result.= str_replace("%%PAGEURL%%",$uriresolver.":".$id,$this->template["attribute"]["intro"]);
 				$result.= str_replace("%%LABEL%%",$label,$this->template["attribute"]["label"]);
 				$goal = "wiki_subattribute('$label',X),write(X),write('%%'),fail.";
 				$res = $utl->call_prolog($goal);
 				if (!(strpos($res,"%%")===false))
 				{
 					$res = rtrim($res,"%");
 					$elems = explode("%%",$res);
 					foreach($elems as $el)
 					{
 						$el = str_replace("special:attribute:","",$el);
 						$el = cleanID($el);
 						$el = "special:attribute:".$el;
 						if (!isset($this->ontology["a"]) || !in_array($el,$this->ontology["a"]))
 							$this->ontology["a"][]=$el;
 						$result.= str_replace("%%ATTURL%%",$uriresolver.":".$el,$this->template["attribute"]["subattribute"]);
 					}			
 				}	
 				$result .= str_replace("%%EXPORTURL%%",$exporturl,$this->template["attribute"]["outro"]);
 				break;	
 		}			
		return $result;
	}
 	
 	/** function for preparing RDF export in Loki */
	function export_rdf($pageid,$maxlevels)
	{
		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl;
			
		//initialize namespaces
			
		$this->namespaces['lokirel']= wl('special:uriresolve','',true).":special:relation:";
		$this->namespaces['lokiatt'] = wl('special:uriresolve','',true).":special:attribute:";
			
		$exportcontent = $this->template["intro"];
		foreach ($this->namespaces as $key=>$value)
			$exportcontent .= str_replace(array("%%NS%%","%%NSURI%%"),array($key,$value),$this->template["namespaces"]);
		$exportcontent .= $this->template["intro2"];
		if (!(strpos($pageid,"special:")===false) && strpos($pageid,"special:")==0)
		{
			if(!(strpos($pageid,"special:category")===false))
				$exportcontent .= $this->export_onto($pageid,'c');
			elseif(!(strpos($pageid,"special:attribute")===false))
				$exportcontent .= $this->export_onto($pageid,'a');
			elseif(!(strpos($pageid,"special:relation")===false))
				$exportcontent .= $this->export_onto($pageid,'r');
		}
		else	$exportcontent .= $this->export_page($pageid,1,$maxlevels);
		
		if ($maxlevels>1 && isset($this->pages[2]))
			$exportcontent .= $this->template["adds"];
		for ($i = 2; $i<=$maxlevels; $i++)
		{
			if(isset($this->pages[$i]))
				foreach($this->pages[$i] as $page)
					$exportcontent .= $this->export_page($page,$i,$maxlevels);
		}
	
		if ($this->ontology["c"] != null || $this->ontology["a"] != null || $this->ontology["r"] != null)
		{
			$exportcontent .= $this->template["onto"];
			if ($this->ontology["c"]!=null)
				for ($i = 0; $i < count($this->ontology["c"]); $i++)
					$exportcontent .= $this->export_onto($this->ontology["c"][$i],'c');
			if($this->ontology["r"]!=null)
				for ($i = 0; $i < count($this->ontology["r"]); $i++)
					$exportcontent .= $this->export_onto($this->ontology["r"][$i],'r');
			if($this->ontology["a"]!=null)
			 	for ($i = 0; $i < count($this->ontology["a"]); $i++)
					$exportcontent .= $this->export_onto($this->ontology["a"][$i],'a');
		}
	
		$exportcontent .= $this->template["outro"];
		return $exportcontent;
	
	}
	
}
?>
