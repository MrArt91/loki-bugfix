<?php

include_once DOKU_INC.'lib/plugins/loki/utl/loki_utl.php';
include_once DOKU_INC.'lib/plugins/loki/config.php';

/**
 * Utility class for Loki - contains functions used for SMW "Ask" queries
 *
 * @author GEIST Research Group
 */
class LokiUtlAsk  {  
 
	var $ask_options = array("format","limit","offset","sort","order","headers","mainlabel","link","default","intro","outro","sep");	
	var $ask_options_limited_values = array(
			"format"  => array("table","broadtable","ul","ol","list","count","csv","debug"),
			"headers" => array("show","hide"),
			"link"	 => array("none","subject","all")
		);
	var $ask_options_multiple_limited_values = array(
			"order"	=> array("asc","ascending","desc","descending","reverse","random"),
		);
	var $ask_options_only_numbers = array("limit","offset");
	var $goal_temp = array(
			"c" => "is_wiki_category(%%SUBJECT%%,%%VALUE%%)",
			"r" => "is_wiki_relation(%%SUBJECT%%,%%PROPERTY%%,%%VALUE%%)%%ADDITIONS%%",
			"a" => "is_wiki_attribute(%%SUBJECT%%,%%PROPERTY%%,%%VALUE%%)%%ADDITIONS%%"
		);
	var $goal_templates = array("%%SUBJECT%%","%%PROPERTY%%","%%VALUE%%","%%ADDITIONS%%");
 
 //////////////////////////////////////////////////////////
 //////////////////////FUNCTIONS///////////////////////////

	/** function initializing $options array */
	private function init_options(){
		$options["format"] = "table";
		$options["limit"] = -1;
		$options["offset"] = 0;
		$options["sort"] = "";
		$options["order"] = "asc";
		$options["headers"] = "show";
		$options["mainlabel"] = "";
		$options["link"] = "all";
		$options["default"] = "";
		$options["intro"] = "";
		$options["outro"] = "";
		$options["sep"] = ",";
	//	$options["searchlabel"] = "... further results";
		return $options;
	}
	
	/** function generating main Prolog goal (getting pages matching the query) */
	private function prepare_main_goal($conditions, $level = ""){
		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl;
	
		$pnum = 1;  // current property number (used for property chains)
		$lnum = $level;  // current property number of subqueries 
		$goal="(";		
		foreach ($conditions as $cond)
		{
			$cond_type = "";
			$cond = trim($cond,"[]\t\r\n\0 \x0B");
			if(strcasecmp($cond,"OR")==0) 
			{
				$goal = rtrim($goal,", ");
				$goal .= " ; "; 
			} elseif (strlen($cond)>0)
			{
				$is_category = stripos($cond,"category:");
				$is_relation = strpos($cond,"::");
				$is_attribute = strpos($cond,":=");
								  
				if(!($is_category === false) && $is_category == 0) 
				{
					$arguments =  explode(":", $cond, 2);
					$cond_type = "c";
					$wiki_function = "wiki_category";
				//	$goal .= "wiki_category(X,'".$arguments[1]."'),";
				}
				elseif (!($is_relation === false) && (($is_attribute === false) || (!($is_attribute === false) && $is_relation<$is_attribute)))
				{
					$arguments =  explode("::", $cond,2);
					$cond_type = "r";
				//	$goal .= "wiki_relation(X,'".$arguments[0]."','".$arguments[1]."'),";
				}	 
				elseif (!($is_attribute === false) && (($is_relation === false) || (!($is_relation === false) && $is_attribute<$is_relation))) 
				{
					$arguments =  explode(":=", $cond,2);
					$cond_type = "a";
				//	$goal .= "wiki_attribute(X,'".$arguments[0]."','".$arguments[1]."'),";
				}
				else  //describing single page
				{
					$goal .= "(";
					$retvals = explode("||",$cond);
					for ($i = 0; $i < count($retvals); $i++)
					{
						$retvals[$i] = trim(strtolower($retvals[$i]));
						$goal .= "X".$level." = '".$retvals[$i]."' ; ";
					}
					$goal = rtrim($goal, "; ");
					$goal .="), ";
				}
				if(in_array($cond_type,array_keys($this->goal_temp))) //for categories, relations and attributes
				{
					$prop = trim($arguments[0]);
					$val = trim($arguments[1]);
					$adds = "";
					
					$vals = preg_split('/(?!\|\|[^(<q)]*<\/q>)\|\|/',$val);	//"or" clauses not in subqueries
					if (count($vals)>1) $goal.="(";
					foreach($vals as $val)
					{
						$subj = "X".$level;
						$val = trim($val);
						$adds="";
						if (strcmp($val,"+") == 0) 
							$val="_";
						elseif (substr($val,0,1) == '<' && $cond_type != "c" && substr($val,0,3)!="<q>"){
							$val = ltrim($val,"< \t\r\n");
							$adds = ", P".$pnum.$subj."@=<'".$val."',";
							$val = "P".$pnum.$subj;
							$pnum++;
						} elseif (substr($val,0,1) == '>' && $cond_type != "c"){
							$val = ltrim($val,"> \t\n\r");
							$adds = ", P".$pnum.$subj."@>='".$val."',";
							$val = "P".$pnum.$subj;
							$pnum++;
						} elseif (substr($val,0,1) == '!' && $cond_type != "c"){
							$val = ltrim($val,"< \t\n\r");
							$adds = ", P".$pnum.$subj."\=@='".$val."',";
							$val = "P".$pnum.$subj;
							$pnum++;
						} elseif (substr($val,0,3) == "<q>"){//&& substr($val,strlen($val)-5) == "</q>") {
							$subq = trim($val,"<>q/ \t\n\r");
							$subqelems = preg_split("/(?!(\]\][oOrR\x0B\t \r\n]*\[\[)[^(<q)]*<\/q>)(\]\][oOrR\x0B\t \r\n]*\[\[)/",$subq,-1,PREG_SPLIT_DELIM_CAPTURE);
							$goal .= $this->prepare_main_goal($subqelems,$lnum."1");
							$val = "X".$lnum."1";
							if(strlen($lnum)==0) $lnum=1;
							else $lnum=substr($lnum,0,strlen($lnum)-2).((int)substr($lnum,strlen($lnum)-1)+1);
						} else {
							$val = "'".$val."'";
						}
						if (!(strpos($prop,".")===false)) 
						{
							$subj = "P".$pnum."X".$level;
							$prop = "'".substr($prop,strrpos($prop,".")+1)."'";
						} elseif (substr($prop,0,1) != "'" && substr($prop,strlen($prop)-1) != "'") $prop = "'".$prop."'";
						$goal .= str_replace($this->goal_templates, array($subj,$prop,$val,$adds), $this->goal_temp[$cond_type])."; ";
				 	}
					$goal = rtrim($goal,";, ");
					if (count($vals)>1) $goal.=")";	
					$goal.=",";	
					
					$prop = trim($arguments[0]);
					if (!(strpos($prop,".")===false)) //property chains
					{
						$adds = "";
						$propss = explode(".",$prop);	
						for ($i=count($propss)-2; $i>0; $i--){
							$val = "P".$pnum."X".$level;
							$prop = "'".$props[$i]."'";
							$subj = "P".($lnum+1)."X".$level;
							$pnum++;
							$goal .= str_replace($this->goal_templates, array($subj,$prop,$val,$adds), $this->goal_temp[$cond_type]).", ";
						}
						$val = "P".$pnum."X".$level;
						$prop = "'".$propss[0]."'";
						$subj = "X".$level;
						$pnum++;
						$goal .= str_replace($this->goal_templates, array($subj,$prop,$val,$adds), $this->goal_temp[$cond_type]).", ";
					}	
				
				}
			}
		}
		$goal = rtrim($goal, ", ");
		$goal .= "), ";
		if ($level == "")
			$goal.="write(X),write('%%'),fail.";
		return $goal;
	}
		
	/** main function for processing SMW ask queries */
	function process_ask($fullask){
		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl;
	
		//gathering WikiData and knowledge defined in loki_utl.pl 
		$scope = TEMP_DIR."/*";
		$hash = md5(serialize($fullask.time()));
		exec('echo ":- style_check(-discontiguous)." > '.TEMP_DIR.'dokuwiki.code'.$hash);
		exec('cat "'.getcwd().'/lib/plugins/loki/loki_utl.pl"  >> '.TEMP_DIR.'dokuwiki.code'.$hash);
		exec('grep ".*" -rh  $(grep -rl ".*" '. TEMP_DIR.' | grep "'.$scope.'")  >> '.TEMP_DIR.'dokuwiki.code'.$hash);
		
		//analysing the "Ask" query
		$fullask=preg_replace("/(#ask:)[\t\n \r]*\[\[/","",$fullask,1);
		$askparts = preg_split("/(\]\][ \t\r\n]*\|)/",$fullask);
		$conditions = preg_split("/(?!(\]\][oOrR\x0B\t \r\n]*\[\[)[^(<q)]*<\/q>)(\]\][oOrR\x0B\t \r\n]*\[\[)/",$askparts[0],-1,PREG_SPLIT_DELIM_CAPTURE); //selecting pages
		$arguments = preg_split("/\|/",$askparts[1]);
		$options = $this->init_options();
		$goal = $this->prepare_main_goal($conditions,""); $goal1 = $goal;
		
		$properties[0] = array("property"=>"-","display"=>"","link"=>"","argument"=>""); 
		$props=1;	
		
		
		foreach ($arguments as $arg)
		{
			$arg=trim($arg);
			if (strpos($arg,"?")==0 && !(strpos($arg,"?")===false)){ //properties to display
				$arg=ltrim($arg,'?'); 
				if(!(stripos($arg,"category")===false) && stripos($arg,"category")==0)
				{
					$tmparg1=explode("=",$arg,2);
					$tmparg2=explode(":",$tmparg1[0],2);
					$opt2 = (count($tmparg2) >1) ? trim($tmparg2[1]) : "";
					$dispname = (count($tmparg1) >1) ? trim($tmparg1[1]) : (strlen($opt2)>0 ? ucfirst($opt2) : "Category");
					$arg = "category";
					$opt1 = "";
				} else 
				{
					$tmparg1=explode("#",$arg,2);
					$opt1 = (count($tmparg1) >1) ? "#".trim($tmparg1[1]) : ""; 
					$tmparg2=explode("=",$tmparg1[0],2);
					$dispname = (count($tmparg2) >1) ? trim($tmparg2[1]) : ucfirst(trim($tmparg2[0]));
					$arg = trim($tmparg2[0]);	
					$opt2 = null;
				}
				$properties[$props] = array("property"=>$arg,"display"=>$dispname,"link"=>$opt1,"argument" => $opt2);
				$props += 1;
			}
			else { //options
				$opt = explode('=',$arg,2);
				$opt[0] = strtolower(trim($opt[0]));
				$opt[1] = trim($opt[1]);
				if (in_array($opt[0],$this->ask_options) && strlen($opt[1])>0)
				{
					if (in_array($opt[0],$this->ask_options_only_numbers))
						$options[$opt[0]] = (int)$opt[1];
					elseif (in_array($opt[0],array_keys($this->ask_options_limited_values)))
					{
						if (in_array($opt[1],$this->ask_options_limited_values[strtolower($opt[0])]))
							$options[$opt[0]] = strtolower($opt[1]);
					}
					elseif (in_array($opt[0],array_keys($this->ask_options_multiple_limited_values)))
					{
						$tmpopt = explode (',',$opt[1]);
						if (count($tmpopt) >0)	 $options[$opt[0]] = "";
						foreach ($tmpopt as $to)
						{
							$to = trim($to);
							$options[$opt[0]] .= (in_array($to,$this->ask_options_multiple_limited_values[strtolower($opt[0])])) ? 
								 strtolower($to)."," : "," ;
						}
						$laco = strrpos($options[$opt[0]],",");
						$options[$opt[0]] = ($laco ===FALSE) ? $options[$opt[0]] : substr($options[$opt[0]],0,$laco);
					}
					
					else
						$options[$opt[0]] = $opt[1];
				}
			}		
		}
	
		// obtain main results		
		$queryresult = $utl->call_prolog($goal,"*",$hash); 
		if (strpos($queryresult,"%%") === false)  //no matches
		{
			$result["options"] = $options;
			$result["properties"] = $properties;
			$result["goal"] = $goal1;
			$result["result"] = strcmp($options["format"],"count")==0 ? "0" : null;
			@unlink(TEMP_DIR."dokuwiki.code".$hash);
			@unlink(TEMP_DIR."dokuwiki.loki".$hash);
			return $result;
		}
		$matches = explode("%%", rtrim($queryresult,"%%"));
		$matches = array_unique($matches);
		usort($matches,array(&$utl, "isort"));
		for ($i=0; $i<count($matches); $i++)
			$result_table[$i][0] = $matches[$i];
		$properties[0]["display"] = (strlen($options["mainlabel"])>0) ? $options["mainlabel"] : "";
		$properties[0]["link"] = strcmp($options["link"],"none")==0 ? "#" : "";
 
		if (strcmp($options["format"],"count")==0)	
		{	  
			$result["options"] = $options;
			$result["result"] = count($matches);
			@unlink(TEMP_DIR."dokuwiki.code".$hash);
			@unlink(TEMP_DIR."dokuwiki.loki".$hash);
			return $result;
		}
		// getting all properties' values
		for ($p = 1; $p <count($properties); $p++)
		{
			
			$property_name = $properties[$p]["property"];
			if (strcmp($property_name,"category")==0)
			{
				if (strlen($properties[$p]["argument"])>0)
				{
					$goal = "wiki_category('%%MATCH%%','".$properties[$p]["argument"]."'),write('X'),!.";
					$properties[$p]["link"] = "#";
				}
				else
				{
					$goal = "wiki_category('%%MATCH%%',X),write(X),write('%%'),fail.";
					$properties[$p]["link"] = strcmp($options["link"],"all")==0 ? "" : "#";
				}
			} else 
			{		  //for relations and attributes
				$is_att = $utl->is_attribute($property_name);
				$is_rel = $utl->is_relation($property_name);
				for($i=0; $i<count($conditions) && $is_att && $is_rel; $i++)
				{
					if (!(strpos(ltrim($conditions[$i]," \n\r\t["), $property_name) === FALSE) && (strpos(ltrim($conditions[$i]," \n\r\t["), $property_name) == 0)){
						$relat = strpos($contitions[$i],"::");
						$attrib = strpos($conditions[$i],":=");
						if (!($relat===false) && $attrib===FALSE) $is_att = false;
						elseif ($relat===false && !($attrib===false)) $ir_rel = false;
						elseif (!($relat===false) && !($attrib===false)) {
							if ($relat < $attrib) $is_att = false; else $is_rel = false;} 
					}			  
				}
				$is_rel = $is_att && $is_rel ? false : $is_rel;
				if ($is_rel)
				{
					$goal = "wiki_relation('%%MATCH%%','".$property_name."',X),write(X),write('%%'),fail.";
					$properties[$p]["link"] = (strcmp($options["link"],"all")==0 && strlen($properties[$p]["link"])==0) ? "" : "#";
				} else 
				{
					$goal = "wiki_attribute('%%MATCH%%','".$property_name."',X),write(X),write('%%'),fail.";
					$properties[$p]["link"] = "#";  
				}
			}
			for ($match = 0; $match < count($matches); $match++)
			{
				$newgoal = str_replace ("%%MATCH%%",$matches[$match],$goal);
				$res = $utl->call_prolog($newgoal,"*",$hash);
				if (strpos($res,"%%") === false && $properties[$p]["argument"]=="") 
				{
					$res = "";
				} else $res = rtrim($res,"%%");
				if (!(strpos($res,"%%") === false))
				{
					$rese = explode ("%%",$res);
					$rese = array_unique($rese);
					usort($rese, array(&$utl, "isort"));
					$res = implode($rese,"%%");
				}
				$result_table[$match][$p] = $res;	//writing property value to result table		  
			}				  
		}	 
		
		//sorting the results if required
		if (strlen($options["sort"])>0 || $options["order"] != "asc")
		{
			if ($options["sort"] !="" || $options["order"]!="ascending")
			{
				$sort = explode(",",$options["sort"]);
				$order = explode(",",$options["order"]);
				
				foreach ($sort as $s)	$s = trim($s);
				$valcount = count($result_table);
							
				if ($order[0]=="random") 
				{
					for ($i = 0; $i < $valcount; $i++)
						$sortval[$i] = mt_rand();
					array_multisort($sortval, $result_table);
				}
				elseif ($options["sort"]=="")
				{	//reverse order
					for ($i = 0; $i < $valcount; $i++)
						$sortval[$i] = $result_table[$i][0];
					array_multisort($sortval, SORT_DESC, $result_table);				
				}
				else
				{
					for ($i = count($sort) - 1 ; $i>=0; $i--)
					{					
						if (array_key_exists($i, $order) == false)
							$order[$i] = "asc";
						if ($order[$i]=="random") 
						{
							for ($k = 0; $k < $valcount; $i++)
								$sortval[$k] = mt_rand();
							array_multisort($sortval, SORT_STRING, $result_table);
						}	
						else										
							for ($k = 0; $k < count($properties); $k++) 
								if ($properties[$k]["property"] == $sort[$i])
								{
									for ($j = 0; $j <$valcount; $j++)
										$sortval[$j] = $result_table[$j][$k];
									if ($order[$i] == "asc" || $order[$i] == "ascending")
										array_multisort($sortval,  $result_table);
									else
										array_multisort($sortval, SORT_DESC, $result_table);
								}
					}			
				}		
			}
		}
		
		$result["goal"] = $goal1;  // for debug
		$result["options"] = $options;
		$result["properties"] = $properties;		
		$result["result"] = $result_table;

		@unlink(TEMP_DIR."dokuwiki.code".$hash);
		@unlink(TEMP_DIR."dokuwiki.loki".$hash);

		return $result;
	}
	
	
	/** function for preparing Ask results based on a given format. The function returns an array of constant strings */
	function prepare_format ($options)
	{
		$r["before_all"] = $r["before_row"] = $r["before_header"] = $r["after_header"] = $r["before_elements"] 
			= $r["before_element"] = $r["after_element"] = $r["after_elements"] = $r["after_row"] = $r["after_all"] = "";
		
		switch ($options["format"])
		{
			case "table":
				$r["before_all"] = "<table cellspacing=\"10px\">";
				$r["before_row"] = "<tr>";
				$r["before_header"] = "<th>";
				$r["after_header"] = "</th>";
				$r["before_element"] = "<td>";
				$r["after_element"] = "</td>";
				$r["after_row"] = "</tr>";
				$r["after_all"] = "</table>";
				break;
			case "broadtable":
				$r["before_all"] = "<table width=\"100%\" cellspacing=\"10px\">";
				$r["before_row"] = "<tr>";
				$r["before_header"] = "<th>";
				$r["after_header"] = "</th>";
				$r["before_element"] = "<td>";
				$r["after_element"] = "</td>";
				$r["after_row"] = "</tr>";
				$r["after_all"] = "</table>";	
				break;
			case "ul":
				$r["before_all"] = "<ul>";
				$r["before_row"] = "<li>";
				$r["after_header"] = " ";
				$r["before_elements"] = " (";
				$r["after_element"] = ", ";
				$r["after_elements"] = ")";
				$r["after_all"] = "</ul>";	
				break;
			case "ol":
				$r["before_all"] = "<ol>";
				$r["before_row"] = "<li>";
				$r["after_header"] = " ";
				$r["before_elements"] = " (";
				$r["after_element"] = ", ";
				$r["after_elements"] = ")";
				$r["after_all"] = "</ol>";
				break;
			case "list":
				$r["before_elements"] = " (";
				$r["after_header"] = " ";
				$r["after_element"] = ", ";
				$r["after_elements"] = ")";	
				$r["after_row"] = str_replace("_"," ",$options["sep"]);
				break;
			case "csv":
				$r["before_element"] = $r["before_header"] = "\"";
				$r["after_element"] = $r["after_header"] = "\"".$options["sep"];
				$r["after_row"] = "\r\n";
				break;
		}
		return $r;
	}
	
}
?>
