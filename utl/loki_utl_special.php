<?php

include_once DOKU_INC.'lib/plugins/loki/utl/loki_utl.php';

/**
 * Utility class for Loki - contains functions used for special wikipages
 *
 * @author GEIST Research Group
 */
class LokiUtlSpecial  {  

 	
	/** function listing all categories used in Loki */
	function list_categories(){
		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl;
		$goal = "wiki_category(_,X),write(X),write('%'),fail.";
	 			
		$result = $utl->call_prolog($goal);			
		if (strpos($result,"%") === false)
		{
			return;
		}
		$pages =  explode("%", rtrim($result,"%"));	
		$ret = array_unique($pages);
		usort($ret,array(&$utl, "isort"));
		return $ret;		
	}

	/** function listing all relations used in Loki */
	function list_relations(){
		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl;
		$goal = "wiki_relation(_,X,_),write(X),write('%'),fail.";
	 			
		$result = $utl->call_prolog($goal);
					
		if (strpos($result,"%") === false)
		{
			return;
		}
				
		$pages =  explode("%", rtrim($result,"%"));	
		$ret = array_unique($pages);
		usort($ret,array(&$utl, "isort"));
		return $ret;		
	}

	/** function listing all attributes used in Loki */
	function list_attributes(){
		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl;
		$goal = "wiki_attribute(_,X,_),write(X),write('%'),fail.";
	 			
		$result = $utl->call_prolog($goal);			
		if (strpos($result,"%") === false)
		{
			return;
		}
				
		$pages =  explode("%", rtrim($result,"%"));	
		$ret = array_unique($pages);
		usort($ret,array(&$utl, "isort"));
		return $ret;		
	}

	/** function listing all category uses in Loki */
	function list_category_uses($category){
		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl;
		$goal = "wiki_category(X,".$category."),write(X),write('%'),fail.";
	 			
		$result = $utl->call_prolog($goal);
		if (strpos($result,"%") === false)
		{
			return;
		}
				
		$pages =  explode("%", rtrim($result,"%"));	
		$ret = array_unique($pages);
		usort($ret,array(&$utl, "isort"));
		return $ret;		
	}

	/** function listing all relation uses in Loki */
	function list_relation_uses($relation){
		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl;
		$goal = "wiki_relation(X,".$relation.",Y),write(X),write('%'),write(Y),write('%%'),fail.";
	 			
		$result = $utl->call_prolog($goal);	
		if (strpos($result,"%%") === false)
		{	
			return;
		}
				
		$pages =  explode("%%", rtrim($result,"%%"));	
		$ret = array_unique($pages);
		usort($ret,array(&$utl, "isort"));
		$i=0;
		foreach($ret as $r){
			$re[$i] = explode("%",$r);
			$i+=1;}
		return $re;		
	}

	/** function listing all attribute uses in Loki */
	function list_attribute_uses($attribute){
		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl;
		$goal = "wiki_attribute(X,".$attribute.",Y),write(X),write('%'),write(Y),write('%%'),fail.";
	 			
		$result = $utl->call_prolog($goal);
		if (strpos($result,"%%") === false)
		{
			return;
		}
				
		$pages =  explode("%%", rtrim($result,"%%"));	
		$ret = array_unique($pages);
		usort($ret,array(&$utl, "isort"));
		$i=0;
		foreach($ret as $r){
			$re[$i] = explode("%",$r);
			$i+=1;}
		return $re;		
	}
	
}
?>
