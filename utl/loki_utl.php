<?php

/**
 * The main utility class for Loki 
 *
 * @author GEIST Research Group
 */
include_once DOKU_INC.'lib/plugins/loki/config.php';

class LokiUtl  {  
 
 
	/** function calling swipl with a given goal. 
		 @param goal - goal for the Prolog engine
		 @param scope - optional scope. Default "*" (all Wiki data).
		 @param hash - optional hash for the file containing Prolog Wiki data. If null then Wiki data is gathered and saved in a temporary file 
	*/
	function call_prolog($goal, $scope="*", $hash=NULL ){
		
		$gather_data = FALSE;
		
		if ($hash==NULL)
		{
			$hash = md5(serialize($goal.time()));
			$gather_data = TRUE;
		}
		$scope = TEMP_DIR."/".$scope;

		if ($gather_data)
		{
			exec('echo ":- style_check(-discontiguous)." > '.TEMP_DIR.'dokuwiki.code'.$hash);
			exec('grep ".*" -rh  $(grep -rl ".*" '. TEMP_DIR.' | grep "'.$scope.'")  >> '.TEMP_DIR.'dokuwiki.code'.$hash);
		}
		
		exec('LANG=pl_PL.utf-8; /usr/bin/swipl -q -s '.TEMP_DIR.'dokuwiki.code'.$hash.' -g "'.$goal.'" -t halt > '.TEMP_DIR.'dokuwiki.loki'.$hash) ;
		$result =  file_get_contents(TEMP_DIR."dokuwiki.loki".$hash);	
		
		if ($gather_data)
		{
			@unlink(TEMP_DIR."dokuwiki.code".$hash);
		}
		@unlink(TEMP_DIR."dokuwiki.loki".$hash);
		
		return $result;
	}
 
	/** function for recursive creating directories */
	function recursive_mkdir($path, $mode = 0777) {
		$newpath = "";
		$path = rtrim($path,"/");
		while (!is_dir($path))
		{
			$newpath = substr($path,strrpos($path,"/")).$newpath;
			$path = substr($path,0,strrpos($path,"/"));
		}
		$newpath = rtrim($newpath,"/");
		$dirs = explode("/",$newpath);
	 	$count = count($dirs); 
		if ($count>0)
			for ($i = 0; $i < $count; $i++) {
				$path .="/" . $dirs[$i];
				if (!is_dir($path) && !mkdir($path, $mode)) {
					return false;
				}
			}
		return true;
	}
	
	/** simple case-insensitive sorting function */
	function isort ($a,$b){
		$aa = strtolower($a);
		$bb = strtolower($b);
		if ($aa==$bb) return 0;
		if ($aa < $bb) return -1;
		return 1;
	}

	/** function checking is a property is an attribute */
	function is_attribute($property, $hash=null){
		$goal = "wiki_attribute(_,'".$property."',_),write('TRUE'),!.";
	 			
		$result = $this->call_prolog($goal, "*", $hash);
		if (strpos($result,"TRUE") === false)
		{
			return false;
		}
		return true;
	}	

	/** function checking is a property is a relation */
	function is_relation($property, $hash=null){
		$goal = "wiki_relation(_,'".$property."',_),write('TRUE'),!.";
	 			
		$result = $this->call_prolog($goal, "*", $hash);
		if (strpos($result,"TRUE") === false)
		{
			return false;
		}
		return true;
	}	
		
 
}
?>
