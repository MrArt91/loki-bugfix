<?php

include_once DOKU_INC.'lib/plugins/loki/utl/loki_utl.php';

/**
 * Utility class for Loki - contains functions used for SPARQL queries
 *
 * @author GEIST Research Group
 */
class LokiUtlSparql  {  
 
 	/** function analyzing variables and constants to display */
 	private function analyze_disp($disp)
 	{
 		$num=0;
 		$res = null;
 		foreach ($disp as $qd)
 		{
 			if (!(strpos($qd,"?")===FALSE) && strpos($qd,"?")==0)
 			{
 				$res[$num]["name"] = $qd;
 				$res[$num]["query"] = ucfirst(substr($qd,1));
 				$res[$num]["type"] = "v";
 				$num++;
 			}
 			else if (!in_array(strtolower($qd),array("distinct","reduced")))
 			{
 				$res[$num]["name"] = $qd;
 				$res[$num]["query"] = "'".$qd."'";
 				$res[$num]["type"] = "c";
 				$num++;
 			} 		
 		}
 		return $res;
 	}
 
 	/** function for preparing single conditions as goal */
 	private function prepare_goal_chunk($cond,$optional,$prefix)
 	{
 		$goal = ""; $i=0;
 		$bits = preg_split("/(?![\s]+[^(<)]*>)[\s]/",$cond,-1,PREG_SPLIT_NO_EMPTY);
 		$cur = "s";
 		foreach ($bits as $b){
 			$b = trim($b);
 			if ($b == ",")
 			{	
 				$i++;
 				$cur = 'o';
 				$tab[$i]["s"] = $tab[$i-1]["s"];
 				$tab[$i]["p"] = $tab[$i-1]["p"];
 				$tab[$i]["type"] = $tab[$i-1]["type"];
 			}elseif ($b==";")
 			{	
 				$i++;
 				$cur = "p";
 				$tab[$i]["s"] = $tab[$i-1]["s"];
 			}elseif ($b==".")
 			{
 				$cur = 's';
 				$i++;
 			} else
 			{
 				if (!(strpos($b,"?")===false) && strpos($b,"?")==0)
 				{
 					$tab[$i][$cur] = ucfirst(trim($b,"?;,"));
 				}
 				elseif (!(strpos($b,"<")===false) && strpos($b,"<")==0)
 				{
 					$tab[$i][$cur] = "'".trim($b,"<>;,")."'";
 					if ($cur=="o" && $tab[$i]["type"] != 'c') $tab[$i]["type"] = "r";
 				}
 				elseif (!(strpos($b,"\"")===false) && strpos($b,"\"")==0)
 				{
 					$tab[$i][$cur] = "'".trim($b,"\";,")."'";
 					if ($cur=="o" && $tab[$i]["type"] != 'c') $tab[$i]["type"] = "a";
 				}
 				elseif ($cur =='p' && ($b=="a" || strtolower($b)=="category"))
 				{
 					$tab[$i][$cur] = "category";
 					$tab[$i]["type"] = "c";
 				}
 				elseif (!(strpos($b,":")===false))
 				{
 					foreach ($prefix as $pp)
	 					if(substr($b,0,strpos($b,":")) == $pp["pref"])
	 					{	
	 						$b=str_replace($pp["pref"].":",$pp["uri"],$b);
	 						break;
	 					}
 					$tab[$i][$cur] = "'".trim($b,",;")."'";
 					if ($cur=="o") $tab[$i]["type"] = "r";
 				}
 				
 				if ($cur == "s") $cur = "p";
 				elseif ($cur=="p") $cur = "o";
 				elseif ($cur=="o") 
 				{
 					if (substr($b,strlen($b)-2,1) == ",")
		 			{
		 				$cur = 'o';
		 				$tab[$i]["s"] = $tab[$i-1]["s"];
		 				$tab[$i]["p"] = $tab[$i-1]["p"];
		 				$tab[$i]["type"] = $tab[$i-1]["type"];
		 			}elseif (substr($b,strlen($b)-2,1)==";")
		 			{
		 				$cur = "p";
		 				$tab[$i]["s"] = $tab[$i-1]["s"];
		 			}elseif(substr($b,strlen($b)-2,1)==".")
		 			{
		 				$cur = "s";
		 				$i++;
		 			}
		 				
 				}
 			}
 		}
 		
 		if (count($tab) >0)
 		foreach ($tab as $ta)
 		{
 			if ($ta["type"] == "c")
 				$curpred = "wiki_category(%subj%,%obj%)";
 			elseif ($ta["type"] == "r")
 				$curpred = "wiki_relation(%subj%,%pred%,%obj%)";
 			elseif ($ta["type"] == "a")
 				$curpred = "wiki_attribute(%subj%,%pred%,%obj%)";
 			elseif (substr($ta["p"],0,1)=="'") 
 			{
 				global $utl;
 				if (!isset($utl)) $utl=new LokiUtl;
 				if ($utl->is_relation(trim($ta["p"],"'")))
 					$curpred = "wiki_relation(%subj%,%pred%,%obj%)";
 				else 
 					$curpred = "wiki_attribute(%subj%,%pred%,%obj%)";
 			}
 			else 
 				$curpred = "(wiki_relation(%subj%,%pred%,%obj%);wiki_attribute(%subj%,%pred%,%obj%))";
 			$curpred = str_replace(array("%subj%","%pred%","%obj%"), array($ta["s"],$ta["p"],$ta["o"]),$curpred);
 			if ($optional==false)
 			{
 				$goal .= $curpred.",";
 			} else 
 			{
 				$goal .= "($curpred;(not($curpred))),";
 			}
 		}
 	//	$goal = rtrim($goal,",");
 		return $goal;
 		
 	}
 	
 	/** function adding a filter to the query */
 	private function create_filter ($condwher,$prefix)
 	{
 		if(count($condwher)==0) return null;
 		$goal="(";
 		foreach ($condwher as $cond)
 		{
 			$cond = trim($cond);
 			foreach($prefix as $pre)
 			{
 				$pr = $pre["pref"].":";
 				$ur = $pre["uri"];
 			}
 			$cond = str_replace($pr,$ur,$cond);
 			if($cond=="&&")
 				$goal.=",";
 			elseif($cond=="||")
 				$goal.=";";
 			elseif((!(stripos($cond,"isiri")===false) && stripos($cond,"isiri")==0) || (!(stripos($cond,"isuri")===false) && stripos($cond,"isuri")==0))	
 			{
 				$arg = trim(substr($cond,5),"() \t\n\r'");
 				if(substr($arg,0,1)=="?") $arg = ucfirst(substr($arg,1));
 				else $arg="'".trim($arg,"<>")."'";
 				$goal.="(wiki_relation(_,_,$arg);wiki_relation($arg,_,_);wiki_attribute($arg,_,_);wiki_category($arg,_))";
 			}
 			elseif(!(stripos($cond,"isliteral")===false) && stripos($cond,"isliteral")==0)
 			{
 				$arg = trim(substr($cond,9),"() \t\n\r'");
 				if(substr($arg,0,1)=="?") $arg = ucfirst(substr($arg,1));
 				else $arg="'".trim($arg,"\"")."'";
 				$goal.="(wiki_attribute(_,_,$arg);wiki_category(_,$arg))";
 			} 	
 			elseif(!(stripos($cond,"samepred")===false) && stripos($cond,"samepred")==0)	
  			{
 				$arg = trim(substr($cond,8),"() \t\n\r'");
 				$cpos = strpos($cond,",");
 				if (!($cpos===false))
 				{
 					$left=ucfirst(substr($arg,1,$cpos-2));
 					$right = ucfirst(trim(substr($arg,$cpos+2),"? "));
 					$goal.="$left=$right";
 				}
 			} 	
 			elseif (!(strpos($cond,">=")===false))	
 			{
 				$args = explode(">=",$cond,2);
				for($i=0; $i<2; $i++)
				{
					$args[$i] = trim($args[$i],"\"\t\r\n '");
	 				if (substr($args[$i],0,1)=="?") $args[$i] = ucfirst(substr($args[$i],1));
	 				else $args[$i] = "'".trim($args[$i],'"')."'"; 
 				}
 				$goal.=$args[0]."@>=".$args[1];
 			}	
 			elseif (!(strpos($cond,"<=")===false))	
 			{
 				$args = explode("<=",$cond,2);
				for($i=0; $i<2; $i++)
				{
					$args[$i] = trim($args[$i],"\"\t\r\n '");
	 				if (substr($args[$i],0,1)=="?") $args[$i] = ucfirst(substr($args[$i],1));
	 				else $args[$i] = "'".trim($args[$i],'"')."'"; 
 				}
 				$goal.=$args[0]."@=<".$args[1];
 			}	
 			elseif (!(strpos($cond,"!=")===false))	
 			{
 				$args = explode("!=",$cond,2);
				for($i=0; $i<2; $i++)
				{
					$args[$i] = trim($args[$i],"\"\t\r\n '");
	 				if (substr($args[$i],0,1)=="?") $args[$i] = ucfirst(substr($args[$i],1));
	 				else $args[$i] = "'".trim($args[$i],'"')."'"; 
 				}
 				$goal.=$args[0]."\\=@=".$args[1];
 			}		
 			elseif (!(strpos($cond,"=")===false))	
 			{
 				$args = explode("=",$cond,2);
				for($i=0; $i<2; $i++)
				{
					$args[$i] = trim($args[$i],"\"\t\r\n '");
	 				if (substr($args[$i],0,1)=="?") $args[$i] = ucfirst(substr($args[$i],1));
	 				else $args[$i] = "'".trim($args[$i],'"')."'"; 
 				}
 				$goal.=$args[0]."=@=".$args[1];
 			}	
 			elseif (!(strpos($cond,">")===false))	
 			{
 				$args = explode(">",$cond,2);
				for($i=0; $i<2; $i++)
				{
					$args[$i] = trim($args[$i],"\"\t\r\n '");
	 				if (substr($args[$i],0,1)=="?") $args[$i] = ucfirst(substr($args[$i],1));
	 				else $args[$i] = "'".trim($args[$i],'"')."'"; 
 				}
 				$goal.=$args[0]."@>".$args[1];
 			}
  			elseif (!(strpos($cond,"<")===false))	
 			{
 				$args = explode("<",$cond,2);
				for($i=0; $i<2; $i++)
				{
					$args[$i] = trim($args[$i],"\"\t\r\n '");
	 				if (substr($args[$i],0,1)=="?") $args[$i] = ucfirst(substr($args[$i],1));
	 				else $args[$i] = "'".trim($args[$i],'"')."'"; 
 				}
 				$goal.=$args[0]."@<".$args[1];
 			}			
 		}
 	
 		$goal.="),";
 		if(strlen($goal)===3) return null;
 		return $goal;
 	}
 
 	/** function for preparing Prolog goal */
 	private function prepare_goal($prefix,$querytype,$querydisp,$where)
 	{
 		if (strlen($where)>0)
		{
			$goal="(";
			$wher=preg_split("/((?![\.]*(([^(\{)]*\})|([^(\<)]*\>)|([^(\()]*\))))[\.])/",$where);
			foreach ($wher as $cond){
				$cond = trim($cond);
				if (!(stripos($cond,"filter")===false) && stripos($cond,"filter")==0)
				{ 
					$cond = ltrim(substr($cond,6));
					$cond = substr($cond,1,strlen($cond)-2);
					$condwher=preg_split("/(\&\&|\|\|)/",$cond,-1, PREG_SPLIT_DELIM_CAPTURE);
					$goal.=$this->create_filter($condwher,$prefix);
				}
				elseif (!(stripos($cond,"optional")===false) && stripos($cond,"optional")==0)
				{
					$cond = ltrim(substr($cond,8));
					$cond = substr($cond,1,strlen($cond)-2);
					$condwher=preg_split("/((?![\.]*(([^(\{)]*\})|([^(\<)]*\>)|([^(\()]*\))))[\.])/",$cond);
					foreach ($condwher as $cw){
						$cw = trim($cw);
						if (!(stripos($cw,"filter")===false) && stripos($cw,"filter")==0)
						{
							$cond = ltrim(substr($cond,6));
							$cond = substr($cond,1,strlen($cond)-2);
							$condwher=preg_split("(\&\&|\|\|)/",$cond,-1, PREG_SPLIT_DELIM_CAPTURE);
							$goal.=$this->create_filter($condwher,$prefix);
						}
						else
						{
							$resu = $this->prepare_goal_chunk($cw,true,$prefix);
							if ($resu != "-1") $goal.=$resu;			
						}
					}
				}
				else
				{
					$resu = $this->prepare_goal_chunk($cond,false,$prefix);
					if ($resu != "-1") $goal.=$resu;
				}
			
			}
			$goal = rtrim($goal,",");
			$goal.="),";
		}
 		if ($querytype == "ask")
		{
			$goal .="write('%%%'),!.";
		}
		elseif($querytype == "select")
		{
			foreach ($querydisp as $qd)
			{
				$goal .= "write(".$qd["query"]."),write('%%'),";
			}
			
			$goal .= "write('%'),fail.";
		}
		elseif ($querytype=="describe")
		{
			$goalst = "((wiki_attribute(SUBJ_,PROP_, VAL_);(wiki_category(SUBJ_, VAL_),PROP_='a');wiki_relation(SUBJ_,PROP_, VAL_)),(";
			foreach ($querydisp as $qd)
			{
				$goalst.="SUBJ_=".$qd["query"].";";
			}
			$goalst = rtrim($goalst,";");
			$goalst.=")),";
			$goal = $goalst.$goal;
			$goal.="write(SUBJ_),write('%%'),write(PROP_),write('%%'),write(VAL_),write('%%%'),fail.";
		}
 		return $goal;
 	}
 
 	/** main function for processing SPARQL queries in Loki */
	function process_sparql($fullquery)
	{
		global $utl;
		if (!isset($utl))
			$utl = new LokiUtl;
		$prefix = $querytype = $querydisp = $where = $order = $limit = $offset = null;	
		$distinct = false;
		$sliced_query = preg_split ("/(prefix|select|ask|construct|describe|where|order by|limit|offset)/i",trim($fullquery), -1, PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY);
		for ($i=0; $i<count($sliced_query); $i+=2){
			$keyword = strtolower($sliced_query[$i]);
			$value = trim($sliced_query[$i+1]);
			if ($keyword == "prefix")
			{
				$pn = strpos($value,"<");
				if(!($pn===false))
					$prefix[] = array("pref" => trim(substr($value,0,$pn-1),"\t\r\n :"), "uri" => trim(substr($value,$pn),"\t\r\n <>"));
			} elseif (in_array($keyword, array("select","ask","construct","describe")))
			{
				$querytype = $keyword;
				if ($querytype!="ask")
				{
					$querydisp = preg_split("/(?![\s]+[^(<)]*>)[\s]/",$value,-1,PREG_SPLIT_NO_EMPTY); //doesn't escape spaces inside quote marks!!
					foreach ($querydisp as $qd)
						$qd = trim($qd);
				} else 
					$where = substr($value,1,strlen($value)-2);
			} elseif ($keyword == "where")
			{
				$where = substr($value,1,strlen($value)-2);
			} elseif ($keyword == "order by") 
			{
				$ord = str_replace(array("\t","\n","\r"),array(" "," "," "),$value);
				$order = explode(" ",$ord);
				$order = array_filter($order);
				$order = array_values($order);
			} elseif ($keyword == "limit")
			{
				$limit = (int)$value;
			} elseif ($keyword == "offset")
			{  
				$offset = (int)$value;
			}	  
		}
		if ((count($querydisp)==0 || strlen($querytype)==0 ||$querytype =="construct") && $querytype !="ask"){ 
			return null;
		}
		if ($querytype=="select" && strtolower($querydisp[0] == "distinct")){
			$distinct = true;
			$querydisp[0] = null;
			$querydisp = array_filter($querydisp);
			$querydisp = array_values($querydisp);			
		}
		if ($querytype != "ask") $querydisp = $this->analyze_disp($querydisp);
		
		$goal = $this -> prepare_goal($prefix, $querytype, $querydisp, $where);  
//return $goal;
		global $msgerr; 
	//str_replace(array("<",">","&"),array("&lt;","&gt;","&amp;"),*/
		$sparqlres = $utl -> call_prolog($goal.$msgerr); 
		if (strpos($sparqlres,"%%%") ===FALSE && $querytype != 'ask')
		{
			return null;
		}
		
		if ($querytype == "ask")
		{
			if (strpos($sparqlres,"%%%") ===false)
				return "no";
			return "yes";		
		}
		$sparqlres = preg_replace("/_G[0-9]+%/"," %",$sparqlres);		
		$sparqlres = rtrim($sparqlres,"%");
		$sparqlres = str_replace(array("<",">","&"),array("&lt;","&gt;","&amp;"),$sparqlres);
		$spres = explode ("%%%",$sparqlres);
		if ($distinct ==true || $querytype == "describe")
			$spres = array_unique($spres);
		usort($spres,array(&$utl, "isort"));
		for($kk = 0; $kk <count($spres); $kk++){
			$spresult[$kk] = explode("%%",$spres[$kk]);}
		if ($querytype=="select")
		{
			if (count($order)>0)
			{ 
				for($i = count($order)-1; $i>=0; $i--)
				{					
					if (!(stripos($order[$i],"desc")===FALSE))
						$asc = false;
					else  
						$asc = true;
					$varst = strpos($order[$i],"?");
					if (!($varst === false))
					{
						$ordp = rtrim(substr($order[$i],$varst),")");
						for ($j = 0; $j<count($querydisp); $j++)
							if ($ordp==$querydisp[$j]["name"])
							{
								foreach ($spresult as $sr) 
									$ordertab[] = $sr[$j]; 
							}
						if ( count($ordertab)==count($spresult))
							if ($asc==true)
								array_multisort($ordertab,$spresult);
							else
								array_multisort($ordertab,SORT_DESC,$spresult);
					}					 
				}
			} 
			if ($limit==null) $limit = count($spresult); 
			if ($offset == null) $offset = 0; else $offset-=1;
			
			$result = "<table border=\"1\" cellspacing=\"3px\" ><tr>";
			foreach ($querydisp as $qdi) 
				$result.="<th>".substr($qdi["name"],1)."</th>";
			$result.= "</tr>";
			for ($k = $offset; $k<($limit+$offset) && $k<count($spresult); $k++)
			{ 
				$result.="<tr>"; 
				foreach($spresult[$k] as $sp)
					$result.="<td>".$sp."</td>";
				$result.="</tr>";
			}
			$result .="</table><br>";
			return $result;
		}
		
		if ($querytype=="describe")
		{
			if (count($spresult)==0) return null;
			$result.="<table border=\"0\" cellspacing=\"3px\">";
			$result .="<tr><td>  ".$spresult[0][0]."  </td><td>  ".$spresult[0][1]."  </td><td>  ".$spresult[0][2];
			for ($i=1; $i<count($spresult); $i++)
			{
				if ($spresult[$i][1] == $spresult[$i-1][1])
					$result .= "  ,</td></tr><tr><td></td><td></td><td>  ".$spresult[$i][2];
				elseif ($spresult[$i][0] == $spresult[$i-1][0])
					$result .= "  ;</td></tr><tr><td></td><td>  ".$spresult[$i][1]."  </td><td>".$spresult[$i][2];
				else 
					$result .= "  .</td></tr></table><table border=\"0\" cellspacing=\"3px\"><tr><tr><td> ".$spresult[$i][0]."  </td><td> ".$spresult[$i][1]."  </td><td>  ".$spresult[$i][2];
			}
			$result.=" .</td></tr></table><br>";
			return $result;
		  
		}
	
	
		return $result;
	}
	
}
?>
