%%%%%%%%%%%%%%%%%%%%%%%%%%
% via dokuwiki
%%%%%%%%%%%%%%%%%%%%%%%%%%

wikiconsult(Scope) :- 
	shell('rm -f lib/plugins/loki/tmp/tempfile.pl'),
     	string_concat('lib/plugins/loki/tmp/loki/',Scope, Res),
    	string_concat('grep ".*" -rh  $(grep ".*" -rl lib/plugins/loki/tmp/loki | grep ', Res,Res2),
        string_concat(Res2, ') > lib/plugins/loki/tmp/tempfile.pl',Res3),
        shell(Res3),true,
        consult('lib/plugins/loki/tmp/tempfile.pl').

list :-
	shell('swipl -q -f lib/plugins/loki/tmp/tempfile.pl -t \'listing.\' | grep "[^ ]*(.*)" | cut -f1 -d "(" | tr -d "[:blank:]" | sort -u ').

list(Pred) :-
	string_concat('swipl -q -f lib/plugins/loki/tmp/tempfile.pl -t \'listing(', Pred , Res),
	string_concat(Res, ')\' ' , Res2),
	shell(Res2).


%%%%%%%%%%%%%%%%%%%%%%%%%%
% via shell
%%%%%%%%%%%%%%%%%%%%%%%%%%

shell_wikiconsult(Path,Scope) :-
      write('PlWiki system.'),nl,
      shell('rm -f tmp/tempfile.pl'),           
      write('Scope set to: '), write(Scope),nl,
      string_concat('lib/plugins/loki/tmp/loki/',Scope, Res),
      string_concat('grep ".*" -rh  $(grep ".*" -rl ',Path,Res2),
      string_concat(Res2,'/lib/plugins/loki/tmp/loki | grep ', Res3),
      string_concat(Res3, Res, Res4),
      string_concat(Res4, ') > ', Res5),
      string_concat(Res5, Path, Res6),
      string_concat(Res6, '/lib/plugins/loki/tmp/tempfile.pl', Res7),
      write('Consulting... '),  write(Res),nl,
      shell(Res7),
      string_concat(Path, '/lib/plugins/loki/tmp/tempfile.pl', Res8),
      consult(Res8),
      write('Consulted.'),nl.
      
shell_wikiconsult :-
      write('Specify Loki absolute path [Ex. \' /var/www/wiki \']'),nl,
      read(Path),
      write('Specify Loki scope [Ex. \' loki/dev \']'),nl,
      read(Scope),
      shell_wikiconsult(Path, Scope).


shell_list :-
	shell('swipl -q -f tmp/tempfile.pl -t \'listing.\' | grep "[^ ]*(.*)" | cut -f1 -d "(" | tr -d "[:blank:]" | sort -u ').

shell_list(Pred) :-
	string_concat('swipl -q -f tmp/tempfile.pl -t \'listing(', Pred , Res),
	string_concat(Res, ')\' ' , Res2),
	shell(Res2).

